/*
 * ParachuteMod.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.parachute.common;

import com.parachute.client.event.ParachuteInputEvent;
import com.parachute.client.event.ParachuteViewRenderEvent;
import com.parachute.client.keybinding.ModKeyBinding;
import com.parachute.client.renderer.HudCompassRenderer;
import com.parachute.client.renderer.ParachuteRenderer;
import com.parachute.client.renderer.PoweredParagliderRenderer;
import com.parachute.common.event.ConfigHandler;
import com.parachute.common.event.PlayerLoginHandler;
import com.parachute.common.event.PlayerTickEventHandler;
import com.parachute.common.event.PoweredParagliderRepairEvent;
import com.parachute.common.network.PacketHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;


@Mod(ParachuteMod.MODID)
public class ParachuteMod {
    public static final String MODID = "parachutemod";

    public ParachuteMod() {
        IEventBus modbus = FMLJavaModLoadingContext.get().getModEventBus();
        modbus.addListener(this::setup);
        modbus.addListener(this::initClient);

        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ConfigHandler.commonSpec);
        MinecraftForge.EVENT_BUS.register(this);

        ModObjects.ENTITY_TYPES.register(modbus);
        ModObjects.ITEMS.register(modbus);
        ModObjects.SOUNDS.register(modbus);
    }

    private void setup(final FMLCommonSetupEvent event) {
        PacketHandler.register();
        MinecraftForge.EVENT_BUS.register(new PlayerTickEventHandler());
        MinecraftForge.EVENT_BUS.register(new PlayerLoginHandler());
        MinecraftForge.EVENT_BUS.register(new PoweredParagliderRepairEvent());
    }

    private void initClient(final FMLClientSetupEvent event) {
        RenderingRegistry.registerEntityRenderingHandler(ModObjects.PARACHUTE.get(), ParachuteRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(ModObjects.POWERED_PARAGLIDER.get(), PoweredParagliderRenderer::new);
        ModKeyBinding.registerKeyBinding();

        MinecraftForge.EVENT_BUS.register(new ParachuteInputEvent());
        MinecraftForge.EVENT_BUS.register(new HudCompassRenderer());
        MinecraftForge.EVENT_BUS.register(new ParachuteViewRenderEvent());
    }

}


