/*
 * ModObjects.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.common;

import com.parachute.common.entity.ParachuteEntity;
import com.parachute.common.entity.PoweredParagliderEntity;
import com.parachute.common.item.ParachuteItem;
import com.parachute.common.item.ParachutePackItem;
import com.parachute.common.item.PoweredParagliderItem;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.function.Supplier;

@SuppressWarnings("unused")
public class ModObjects {
    public static final String PARACHUTE_NAME = "parachute";
    public static final String POWERED_PARAGLIDER_NAME = "powered_paraglider";

    public static final String BLACK_PARACHUTE_NAME = "black_parachute";
    public static final String BLUE_PARACHUTE_NAME = "blue_parachute";
    public static final String BROWN_PARACHUTE_NAME = "brown_parachute";
    public static final String CYAN_PARACHUTE_NAME = "cyan_parachute";
    public static final String GRAY_PARACHUTE_NAME = "gray_parachute";
    public static final String GREEN_PARACHUTE_NAME = "green_parachute";
    public static final String LIGHT_BLUE_PARACHUTE_NAME = "light_blue_parachute";
    public static final String LIGHT_GRAY_PARACHUTE_NAME = "light_gray_parachute";
    public static final String LIME_PARACHUTE_NAME = "lime_parachute";
    public static final String MAGENTA_PARACHUTE_NAME = "magenta_parachute";
    public static final String ORANGE_PARACHUTE_NAME = "orange_parachute";
    public static final String PINK_PARACHUTE_NAME = "pink_parachute";
    public static final String PURPLE_PARACHUTE_NAME = "purple_parachute";
    public static final String RED_PARACHUTE_NAME = "red_parachute";
    public static final String WHITE_PARACHUTE_NAME = "white_parachute";
    public static final String YELLOW_PARACHUTE_NAME = "yellow_parachute";
    public static final String CAMO_PARACHUTE_NAME = "camo_parachute";
    public static final String RAINBOW_PARACHUTE_NAME = "rainbow_parachute";

    public static final String BLACK_POWERED_PARAGLIDER_NAME = "black_powered_paraglider";
    public static final String BLUE_POWERED_PARAGLIDER_NAME = "blue_powered_paraglider";
    public static final String BROWN_POWERED_PARAGLIDER_NAME = "brown_powered_paraglider";
    public static final String CYAN_POWERED_PARAGLIDER_NAME = "cyan_powered_paraglider";
    public static final String GRAY_POWERED_PARAGLIDER_NAME = "gray_powered_paraglider";
    public static final String GREEN_POWERED_PARAGLIDER_NAME = "green_powered_paraglider";
    public static final String LIGHT_BLUE_POWERED_PARAGLIDER_NAME = "light_blue_powered_paraglider";
    public static final String LIGHT_GRAY_POWERED_PARAGLIDER_NAME = "light_gray_powered_paraglider";
    public static final String LIME_POWERED_PARAGLIDER_NAME = "lime_powered_paraglider";
    public static final String MAGENTA_POWERED_PARAGLIDER_NAME = "magenta_powered_paraglider";
    public static final String ORANGE_POWERED_PARAGLIDER_NAME = "orange_powered_paraglider";
    public static final String PINK_POWERED_PARAGLIDER_NAME = "pink_powered_paraglider";
    public static final String PURPLE_POWERED_PARAGLIDER_NAME = "purple_powered_paraglider";
    public static final String RED_POWERED_PARAGLIDER_NAME = "red_powered_paraglider";
    public static final String WHITE_POWERED_PARAGLIDER_NAME = "white_powered_paraglider";
    public static final String YELLOW_POWERED_PARAGLIDER_NAME = "yellow_powered_paraglider";
    public static final String CAMO_POWERED_PARAGLIDER_NAME = "camo_powered_paraglider";
    public static final String RAINBOW_POWERED_PARAGLIDER_NAME = "rainbow_powered_paraglider";

    public static final String PACK_NAME = "pack";

    public static final String CHUTE_OPEN_SOUND = "chuteopen";
    public static final String CHUTE_LIFT_SOUND = "lift";

    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES = new DeferredRegister<>(ForgeRegistries.ENTITIES, ParachuteMod.MODID);
    public static final DeferredRegister<Item> ITEMS = new DeferredRegister<>(ForgeRegistries.ITEMS, ParachuteMod.MODID);
    public static final DeferredRegister<SoundEvent> SOUNDS = new DeferredRegister<>(ForgeRegistries.SOUND_EVENTS, ParachuteMod.MODID);

    public static final RegistryObject<EntityType<ParachuteEntity>> PARACHUTE = register(ModObjects::parachute_entity, PARACHUTE_NAME);
    public static final RegistryObject<EntityType<PoweredParagliderEntity>> POWERED_PARAGLIDER = register(ModObjects::powered_paraglider_entity, POWERED_PARAGLIDER_NAME);

    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_BLACK = register(BLACK_PARACHUTE_NAME, ModObjects::parachute_item_black);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_BLUE = register(BLUE_PARACHUTE_NAME, ModObjects::parachute_item_blue);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_BROWN = register(BROWN_PARACHUTE_NAME, ModObjects::parachute_item_brown);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_CYAN = register(CYAN_PARACHUTE_NAME, ModObjects::parachute_item_cyan);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_GRAY = register(GRAY_PARACHUTE_NAME, ModObjects::parachute_item_gray);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_GREEN = register(GREEN_PARACHUTE_NAME, ModObjects::parachute_item_green);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_LIGHT_BLUE = register(LIGHT_BLUE_PARACHUTE_NAME, ModObjects::parachute_item_light_blue);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_LIGHT_GRAY = register(LIGHT_GRAY_PARACHUTE_NAME, ModObjects::parachute_item_light_gray);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_LIME = register(LIME_PARACHUTE_NAME, ModObjects::parachute_item_lime);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_MAGENTA = register(MAGENTA_PARACHUTE_NAME, ModObjects::parachute_item_magenta);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_ORANGE = register(ORANGE_PARACHUTE_NAME, ModObjects::parachute_item_orange);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_PINK = register(PINK_PARACHUTE_NAME, ModObjects::parachute_item_pink);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_PURPLE = register(PURPLE_PARACHUTE_NAME, ModObjects::parachute_item_purple);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_RED = register(RED_PARACHUTE_NAME, ModObjects::parachute_item_red);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_WHITE = register(WHITE_PARACHUTE_NAME, ModObjects::parachute_item_white);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_YELLOW = register(YELLOW_PARACHUTE_NAME, ModObjects::parachute_item_yellow);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_CAMO = register(CAMO_PARACHUTE_NAME, ModObjects::parachute_item_camo);
    public static final RegistryObject<ParachuteItem> PARACHUTE_ITEM_RAINBOW = register(RAINBOW_PARACHUTE_NAME, ModObjects::parachute_item_rainbow);

    public static final RegistryObject<PoweredParagliderItem> POWERED_PARACHUTE_ITEM_BLACK = register(BLACK_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_black);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_BLUE = register(BLUE_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_blue);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_BROWN = register(BROWN_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_brown);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_CYAN = register(CYAN_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_cyan);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_GRAY = register(GRAY_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_gray);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_GREEN = register(GREEN_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_green);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_LIGHT_BLUE = register(LIGHT_BLUE_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_light_blue);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_LIGHT_GRAY = register(LIGHT_GRAY_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_light_gray);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_LIME = register(LIME_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_lime);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_MAGENTA = register(MAGENTA_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_magenta);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_ORANGE = register(ORANGE_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_orange);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_PINK = register(PINK_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_pink);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_PURPLE = register(PURPLE_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_purple);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_RED = register(RED_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_red);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_WHITE = register(WHITE_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_white);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_YELLOW = register(YELLOW_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_yellow);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_CAMO = register(CAMO_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_camo);
    public static final RegistryObject<PoweredParagliderItem> POWERED_PARAGLIDER_ITEM_RAINBOW = register(RAINBOW_POWERED_PARAGLIDER_NAME, ModObjects::powered_paraglider_item_rainbow);

    public static final RegistryObject<ParachutePackItem> PARACHUTEPACK_ITEM = register(PACK_NAME, ModObjects::parachute_pack);

    public static final RegistryObject<SoundEvent> OPENCHUTE = register(CHUTE_OPEN_SOUND);
    public static final RegistryObject<SoundEvent> LIFTCHUTE = register(CHUTE_LIFT_SOUND);

    private static RegistryObject<SoundEvent> register(String name) {
        return SOUNDS.register(name, () -> new SoundEvent(new ResourceLocation(ParachuteMod.MODID, name)));
    }

    private static <E extends Entity> RegistryObject<EntityType<E>> register(final Supplier<EntityType.Builder<E>> supplier, final String name) {
        return ENTITY_TYPES.register(name, () -> supplier.get().build(name));
    }

    private static <T extends Item> RegistryObject<T> register(final String name, final Supplier<T> supplier) {
        return ITEMS.register(name, supplier);
    }

    private static EntityType.Builder<ParachuteEntity> parachute_entity() {
        return EntityType.Builder.create(ParachuteEntity::create, EntityClassification.MISC)
                .setCustomClientFactory(((spawnEntity, world) -> PARACHUTE.get().create(world)))
                .setTrackingRange(32)
                .setUpdateInterval(3)
                .setShouldReceiveVelocityUpdates(true)
                .size(3.25f, (1.0f / 16.0f));
    }

    private static EntityType.Builder<PoweredParagliderEntity> powered_paraglider_entity() {
        return EntityType.Builder.create(PoweredParagliderEntity::create, EntityClassification.MISC)
                .setCustomClientFactory(((spawnEntity, world) -> POWERED_PARAGLIDER.get().create(world)))
                .setTrackingRange(32)
                .setUpdateInterval(3)
                .setShouldReceiveVelocityUpdates(true)
                .size(3.5f, (1.0f / 16.0f));
    }

    public static final ItemGroup PARACHUTE_GROUP = new ItemGroup(12, ModObjects.PARACHUTE_NAME) {
        @OnlyIn(Dist.CLIENT)
        @Override
        public ItemStack createIcon() {
            return new ItemStack(PARACHUTE_ITEM_RAINBOW.get());
        }
    };

    private static ParachuteItem parachute_item_black() {
        return new ParachuteItem(ParachuteEntity.Color.BLACK, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_blue() {
        return new ParachuteItem(ParachuteEntity.Color.BLUE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_brown() {
        return new ParachuteItem(ParachuteEntity.Color.BROWN, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_cyan() {
        return new ParachuteItem(ParachuteEntity.Color.CYAN, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_gray() {
        return new ParachuteItem(ParachuteEntity.Color.GRAY, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_green() {
        return new ParachuteItem(ParachuteEntity.Color.GREEN, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_light_blue() {
        return new ParachuteItem(ParachuteEntity.Color.LIGHT_BLUE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_light_gray() {
        return new ParachuteItem(ParachuteEntity.Color.LIGHT_GRAY, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_lime() {
        return new ParachuteItem(ParachuteEntity.Color.LIME, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_magenta() {
        return new ParachuteItem(ParachuteEntity.Color.MAGENTA, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_orange() {
        return new ParachuteItem(ParachuteEntity.Color.ORANGE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_pink() {
        return new ParachuteItem(ParachuteEntity.Color.PINK, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_purple() {
        return new ParachuteItem(ParachuteEntity.Color.PURPLE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_red() {
        return new ParachuteItem(ParachuteEntity.Color.RED, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_white() {
        return new ParachuteItem(ParachuteEntity.Color.WHITE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_yellow() {
        return new ParachuteItem(ParachuteEntity.Color.YELLOW, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_camo() {
        return new ParachuteItem(ParachuteEntity.Color.CAMO, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    private static ParachuteItem parachute_item_rainbow() {
        return new ParachuteItem(ParachuteEntity.Color.RAINBOW, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARACHUTE_MAX_DAMAGE));
    }

    // max damage should match fuel units
    private static PoweredParagliderItem powered_paraglider_item_black() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.BLACK, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_blue() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.BLUE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_brown() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.BROWN, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_cyan() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.CYAN, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_gray() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.GRAY, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_green() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.GREEN, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_light_blue() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.LIGHT_BLUE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_light_gray() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.LIGHT_GRAY, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_lime() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.LIME, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_magenta() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.MAGENTA, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_orange() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.ORANGE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_pink() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.PINK, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_purple() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.PURPLE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_red() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.RED, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_white() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.WHITE, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_yellow() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.YELLOW, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_camo() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.CAMO, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static PoweredParagliderItem powered_paraglider_item_rainbow() {
        return new PoweredParagliderItem(PoweredParagliderEntity.Color.RAINBOW, new Item.Properties().maxStackSize(1).group(PARACHUTE_GROUP).maxDamage(Consts.PARAGLIDER_MAX_DAMAGE));
    }

    private static ParachutePackItem parachute_pack() {
        return new ParachutePackItem(new Item.Properties().maxStackSize(1));
    }

}
