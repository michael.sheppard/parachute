/*
 * PoweredParagliderEntity.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.common.entity;

import com.parachute.common.Consts;
import com.parachute.common.event.ConfigHandler;
import com.parachute.common.ModObjects;
import com.parachute.common.ParachuteMod;
import com.parachute.common.item.PoweredParagliderItem;
import com.parachute.common.network.ClientFuelUpdateMessage;
import com.parachute.common.network.PacketHandler;
import com.parachute.common.network.ServerAscendModeMessage;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.*;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nonnull;
import java.util.List;


public class PoweredParagliderEntity extends Entity {
    private static final DataParameter<Integer> PARAGLIDER_COLOR = EntityDataManager.createKey(PoweredParagliderEntity.class, DataSerializers.VARINT);

    private static double deltaRotation;
    private static boolean allowFuelBurn;
    private static double curLavaDistance;
    private static boolean rideInWater;
    private static boolean ascendMode;

    private static ItemStack gliderItemStack;
    private static int fuelValue;

    private boolean leftKeyDown;
    private boolean rightKeyDown;
    private boolean forwardKeyDown;
    private boolean backKeyDown;
    private boolean jumpKeyDown;

    public static boolean leftTurn;
    public static boolean rightTurn;
    public static double forwardMotion;

    private int turnProgress;
    private double gliderX;
    private double gliderY;
    private double gliderZ;
    private double gliderYaw;
    private double gliderPitch;

    @OnlyIn(Dist.CLIENT)
    private double velocityX;
    @OnlyIn(Dist.CLIENT)
    private double velocityY;
    @OnlyIn(Dist.CLIENT)
    private double velocityZ;


    public static PoweredParagliderEntity create(EntityType<? extends PoweredParagliderEntity> type, World world) {
        return new PoweredParagliderEntity(type, world);
    }

    public PoweredParagliderEntity(EntityType<? extends PoweredParagliderEntity> glider, World world) {
        super(glider, world);

        allowFuelBurn = ConfigHandler.allowFuelBurn;
        rideInWater = ConfigHandler.rideInWater;

        deltaRotation = 0.0;
        curLavaDistance = Consts.MIN_LAVA_DISTANCE;
        this.world = world;
        preventEntitySpawning = true;
        ascendMode = false;
        setSilent(false);
    }

    public PoweredParagliderEntity(World world, double x, double y, double z) {
        this(ModObjects.POWERED_PARAGLIDER.get(), world);
        setPosition(x, y, z);
        setMotion(Vec3d.ZERO);
        prevPosX = x;
        prevPosY = y;
        prevPosZ = z;
    }

    public void setItemstack(ItemStack itemStack) {
        gliderItemStack = itemStack;
    }

    public void setFuelAmount(int amount) {
        fuelValue = amount;
    }

    public static void setAscendMode(boolean mode) {
        ascendMode = mode;
    }

    @Override
    public boolean canRiderInteract() {
        return true;
    }

    @Override
    public AxisAlignedBB getCollisionBox(@Nonnull Entity entity) {
        if (entity != getControllingPassenger() && entity.getRidingEntity() != this) {
            return entity.getBoundingBox();
        }
        return null;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox() {
        return getBoundingBox();
    }

    @Override
    public void applyEntityCollision(@Nonnull Entity entity) {
        if (entity instanceof PoweredParagliderEntity) {
            if (entity.getBoundingBox().minY < getBoundingBox().maxY) {
                super.applyEntityCollision(entity);
            }
        } else if (entity.getBoundingBox().minY <= getBoundingBox().minY) {
            super.applyEntityCollision(entity);
        }
    }

    // pilot should hang when on the paraglider and then
    // pick up legs when landing.
    @Override
    public boolean shouldRiderSit() {
        Entity pilot = getControllingPassenger();
        if (pilot != null) {
            return shouldRiderPickupLegs(new BlockPos(pilot));
        }
        return false;
    }

    // ray tracing for shouldRiderSit method
    private boolean shouldRiderPickupLegs(BlockPos bp) {
        Vec3d v1 = new Vec3d(bp.getX(), bp.getY(), bp.getZ());
        Vec3d v2 = new Vec3d(bp.getX(), bp.down(5).getY(), bp.getZ());
        RayTraceResult traceResult = world.rayTraceBlocks(new RayTraceContext(v1, v2, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.ANY, this));
        if (traceResult.getType() == RayTraceResult.Type.BLOCK) {
            BlockPos blockpos = new BlockPos(traceResult.getHitVec().x, traceResult.getHitVec().y, traceResult.getHitVec().z);
            return !world.getBlockState(blockpos).isAir(world, blockpos);
        }
        return false;
    }

    @Nonnull
    @Override
    public Direction getAdjustedHorizontalFacing() {
        return getHorizontalFacing().rotateY();
    }

    @Override
    public boolean canPassengerSteer() {
        return true;
    }

    @Override
    @Nonnull
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public Entity getControllingPassenger() {
        List<Entity> list = getPassengers();
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public boolean attackEntityFrom(@Nonnull DamageSource source, float amount) {
        return super.attackEntityFrom(source, amount);
    }

    @Override
    public boolean canBeRiddenInWater(Entity pilot) {
        return rideInWater;
    }

    @Override
    protected boolean canBeRidden(@Nonnull Entity entity) {
        return true;
    }

    @Override
    public double getMountedYOffset() {
        return -Consts.GLIDER_OFFSET;
    }

    @Override
    public boolean canBeCollidedWith() {
        return isAlive();
    }

    @Override
    protected void readAdditional(@Nonnull CompoundNBT compoundNBT) {
    }

    @Override
    protected void writeAdditional(@Nonnull CompoundNBT compoundNBT) {
    }

    @Override
    protected void addPassenger(@Nonnull Entity passenger) {
        super.addPassenger(passenger);
        setPositionAndRotation(getPosV().x, getPosV().y, getPosV().z, rotationYaw, rotationPitch);
    }

    // updateInputs is called in ParachuteInputEvent class
    @OnlyIn(Dist.CLIENT)
    public void updateInputs(boolean leftKeyDown, boolean rightKeyDown, boolean forwardKeyDown, boolean backKeyDown, boolean jumpKeyDown) {
        this.leftKeyDown = leftKeyDown;
        this.rightKeyDown = rightKeyDown;
        this.forwardKeyDown = forwardKeyDown;
        this.backKeyDown = backKeyDown;
        this.jumpKeyDown = jumpKeyDown;
    }

    // this method is called on the client so we need to update the server
    private void controlParaglider() {
        double motionFactor = 0.0f;

        if (forwardKeyDown) {
            motionFactor += Consts.FORWARD_MOMENTUM;
        }

        if (backKeyDown) {
            motionFactor -= Consts.BACK_MOMENTUM;
        }

        if (leftKeyDown) {
            deltaRotation -= Consts.ROTATION_MOMENTUM;
        }
        if (rightKeyDown) {
            deltaRotation += Consts.ROTATION_MOMENTUM;
        }

        // slight forward momentum while turning
        if (rightKeyDown != leftKeyDown && !forwardKeyDown && !backKeyDown) {
            motionFactor += Consts.SLIDE_MOMENTUM;
        }

        leftTurn = leftKeyDown;
        rightTurn = rightKeyDown;
        forwardMotion = motionFactor;

        ascendMode = jumpKeyDown;
        rotationYaw += deltaRotation;
        // update ascendMode on the server
        PacketHandler.HANDLER.sendToServer(new ServerAscendModeMessage(ascendMode));

        double motionY = currentDescentRate();
        double motionX = Math.sin((float) Math.toRadians(-rotationYaw)) * motionFactor;
        double motionZ = Math.cos((float) Math.toRadians(rotationYaw)) * motionFactor;
        setMotion(getMotion().add(motionX, motionY, motionZ));

        if (isBadWeather() && rand.nextBoolean()) {
            applyTurbulence(world.isThundering());
        }
    }

    @Override
    protected void registerData() {
        dataManager.register(PARAGLIDER_COLOR, PoweredParagliderEntity.Color.BLACK.ordinal());
    }

    public void setPoweredParagliderColor(PoweredParagliderEntity.Color color) {
        dataManager.set(PARAGLIDER_COLOR, color.ordinal());
    }

    public PoweredParagliderEntity.Color getPoweredParagliderColor() {
        return PoweredParagliderEntity.Color.byID(dataManager.get(PARAGLIDER_COLOR));
    }

    @Override
    public void tick() {
        Entity pilot = getControllingPassenger();
        // the player has pressed LSHIFT or been killed,
        // may be necessary for LSHIFT to kill the paraglider
        if (pilot == null && !world.isRemote) { // server side
            remove();
            return;
        }

        generateContrails(ascendMode);

        prevPosX = getPosV().x;
        prevPosY = getPosV().y;
        prevPosZ = getPosV().z;

        super.tick();
        setPacketCoordinates(getPosV().x, getPosV().y, getPosV().z);
        if (canPassengerSteer()) {
            updateMotion();

            // if we are ascending, we are using fuel.
            if (allowFuelBurn && ascendMode && (fuelValue > 0)) {
                fuelValue--;
                // fuel and damage should sync
                if (!world.isRemote) {
                    gliderItemStack.setDamage(Consts.PARAGLIDER_MAX_DAMAGE - fuelValue);
                    PoweredParagliderItem.setFuelAmount(fuelValue);
                    if (pilot != null) {
                        PacketHandler.HANDLER.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity) pilot), new ClientFuelUpdateMessage(fuelValue));
                    }
                }
                // play the lift sound. kinda like a hot air balloon's burner sound
                SoundEvent sound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(ParachuteMod.MODID, ModObjects.CHUTE_LIFT_SOUND));
                if (sound != null && pilot != null) {
                    pilot.playSound(sound, 1.0f, 1.0F / (rand.nextFloat() * 0.4F + 0.8F));
                }
            }

            setPosition(getPosV().x, getPosV().y, getPosV().z);
            // apply momentum/decay
            Vec3d curMotion = getMotion();
            setMotion(curMotion.x * Consts.DECAY_MOMENTUM, curMotion.y * (curMotion.y < 0.0 ? 0.96 : 0.98), curMotion.z * Consts.DECAY_MOMENTUM);
            deltaRotation *= 0.9;

            if (world.isRemote) {
                controlParaglider();
            }

            // move the paraglider with the motion equations applied
            move(MoverType.SELF, getMotion());
        }

        // something bad happened, somehow the pilot was killed.
        if (!world.isRemote && pilot != null && !pilot.isAlive()) { // server side
            remove();
        }

        doBlockCollisions();
    }

    private void updateMotion() {
        if (canPassengerSteer()) {
            turnProgress = 0;
            setPacketCoordinates(getPosV().x, getPosV().y, getPosV().z);
        }
        if (turnProgress > 0) {
            double dx = getPosV().x + (gliderX - getPosV().x) / (double) turnProgress;
            double dy = getPosV().y + (gliderY - getPosV().y) / (double) turnProgress;
            double dz = getPosV().z + (gliderZ - getPosV().z) / (double) turnProgress;
            double delta_r = MathHelper.wrapDegrees(gliderYaw - (double) rotationYaw);
            rotationYaw = (float) ((double) rotationYaw + delta_r / (double) turnProgress);
            rotationPitch = (float) ((double) rotationPitch + (gliderPitch - (double) rotationPitch) / (double) turnProgress);
            --turnProgress;
            setPosition(dx, dy, dz);
            setRotation(rotationYaw, rotationPitch);
        }
    }

    // check for bad weather, if the biome can rain or snow check to see
    // if it is raining (or snowing) or thundering.
    private boolean isBadWeather() {
        BlockPos bp = new BlockPos(getPosV());
        Chunk chunk = world.getChunkAt(bp);
        boolean canSnow = chunk.getWorld().getBiome(bp).getPrecipitation() == Biome.RainType.SNOW;
        boolean canRain = chunk.getWorld().getBiome(bp).getPrecipitation() == Biome.RainType.RAIN;
        return (canRain || canSnow) && (world.isRaining() || world.isThundering());
    }

    // determines the descent rate based on whether or not
    // the space bar has been pressed. weather and lava affect
    // the final result.
    private double currentDescentRate() {
        double descentRate;

        descentRate = calcHeatSourceThermals();
        if (!allowFuelBurn) {
            return descentRate;
        }

        descentRate += (world.isRaining() ? 0.002 : world.isThundering() ? 0.004 : 0.0);

        if (ascendMode && fuelValue > 0) {
            descentRate = Consts.ASCEND;
        }

        if (getPosV().y >= Consts.MAX_ALTITUDE) {
            descentRate = Consts.DRIFT;
        }

        return -descentRate;
    }

    private Vec3d getPosV() {
        return getPositionVec();
    }

    // the following three methods detect lava|fire below the player
    // at up to 'maxLavaDistance' blocks. added logic to detect campfire.
    private boolean isHeatSource(BlockPos bp) {
        Block block = world.getBlockState(bp).getBlock();
        if (block == Blocks.CAMPFIRE) {
            return true;
        }
        return world.isFlammableWithin(new AxisAlignedBB(bp).expand(0, 1, 0));
    }

    private boolean isHeatSourceInRange(BlockPos bp) {
        Vec3d v1 = new Vec3d(getPosV().x, getPosV().y, getPosV().z);
        Vec3d v2 = new Vec3d(bp.getX(), bp.getY(), bp.getZ());
        RayTraceResult traceResult = world.rayTraceBlocks(new RayTraceContext(v1, v2, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.ANY, this));
        if (traceResult.getType() == RayTraceResult.Type.BLOCK) {
            BlockPos blockpos = new BlockPos(traceResult.getHitVec().x, traceResult.getHitVec().y, traceResult.getHitVec().z);
            return isHeatSource(blockpos);
        }
        return false;
    }

    private double calcHeatSourceThermals() {
        double thermals = Consts.DRIFT;
        final double inc = 0.5;

        if (isHeatSourceInRange(new BlockPos(getPosV().x, getPosV().y - Consts.GLIDER_OFFSET - Consts.MAX_LAVA_DISTANCE, getPosV().z))) {
            curLavaDistance += inc;
            thermals = Consts.ASCEND;
            if (curLavaDistance >= Consts.MAX_LAVA_DISTANCE) {
                curLavaDistance = Consts.MIN_LAVA_DISTANCE;
                thermals = Consts.DRIFT;
            }
        } else {
            curLavaDistance = Consts.MIN_LAVA_DISTANCE;
        }
        return thermals;
    }

    // apply 'turbulence' in the form of a collision force
    private void applyTurbulence(boolean roughWeather) {
        double rmin = 0.1;
        double dPos = rmin + 0.9 * rand.nextDouble();

        if (dPos >= 0.20) {
            double rmax = roughWeather ? 0.8 : 0.5;
            rmax = (rand.nextInt(5) == 0) ? 1.0 : rmax;  // gusting
            double dX = rmin + (rmax - rmin) * rand.nextDouble();
            double dY = rmin + 0.2 * rand.nextDouble();
            double dZ = rmin + (rmax - rmin) * rand.nextDouble();

            dPos = Math.sqrt(dPos);
            double dPosInv = 1.0 / dPos;

            dX /= dPos;
            dY /= dPos;
            dZ /= dPos;

            dPosInv = Math.min(dPosInv, 1.0);

            dX *= dPosInv;
            dY *= dPosInv;
            dZ *= dPosInv;

            dX *= 0.05;
            dY *= 0.05;
            dZ *= 0.05;

            if (rand.nextBoolean()) {
                addVelocity(-dX, -dY, -dZ);
            } else {
                addVelocity(dX, dY, dZ);
            }
        }
    }

    // generate condensation trails at the trailing edge
    // of the paraglider. Yes, I know that most paragliders
    // don't generate contrails (no engines), but most worlds
    // aren't made of block with cubic cows either. If you
    // like, you can think of the trails as chemtrails.
    private void generateContrails(boolean ascending) {
        Vec3d motionVec = getMotion();
        double velocity = Math.sqrt(motionVec.x * motionVec.x + motionVec.z * motionVec.z);
        double cosYaw = 2.25 * Math.cos(Math.toRadians(90.0 + rotationYaw));
        double sinYaw = 2.25 * Math.sin(Math.toRadians(90.0 + rotationYaw));

        for (int k = 0; (double) k < 1.0 + velocity; k++) {
            double sign = (rand.nextInt(2) * 2 - 1) * 1.25;
            double x = getPosV().x + (getPosV().x - prevPosX) + cosYaw * -0.25 + sinYaw * sign;
            double y = getPosV().y - 0.25;
            double z = getPosV().z + (getPosV().z - prevPosZ) + sinYaw * -0.25 - cosYaw * sign;

            if (ascending) {
                world.addParticle(ParticleTypes.LARGE_SMOKE, x, y, z, motionVec.x, motionVec.y, motionVec.z);
            }
            if (!ascending && velocity > 0.01) {
                world.addParticle(ParticleTypes.CLOUD, x, y, z, motionVec.x, motionVec.y, motionVec.z);
            }
        }
    }

    @Override
    public void updatePassenger(@Nonnull Entity passenger) {
        if (isPassenger(passenger)) {
            float offset = (float) (getMountedYOffset() + passenger.getYOffset());
            Vec3d vec3d = (new Vec3d(0.0, 0.0, 0.0)).rotateYaw(-rotationYaw * ((float) Math.PI / 180F) - ((float) Math.PI / 2F));
            passenger.setPosition(getPosV().x + vec3d.x, getPosV().y + (double) offset, getPosV().z + vec3d.z);
            passenger.rotationYaw += deltaRotation;
            passenger.setRotationYawHead(passenger.getRotationYawHead() + (float) deltaRotation);
            applyYawToEntity(passenger);

            // check for passenger collisions
            checkForPlayerCollisions(passenger);
        }
    }

    // check for player colliding with block below. remove() the glider if block is not air, water,
    // grass/vines, or snow/snow layers
    private void checkForPlayerCollisions(Entity passenger) {
        AxisAlignedBB bb = passenger.getBoundingBox();
        if (!world.isRemote && world.checkBlockCollision(bb)) {
            // if in water check dismount-in-water flag, also check for solid block below water
            if (world.isMaterialInBB(bb, Material.WATER)) {
                if (!rideInWater) {
                    remove();
                } else {
                    BlockPos bp = new BlockPos(passenger).down(Math.round((float) bb.minY));
                    if (!world.getBlockState(bp.down(Math.round((float) bb.minY))).isSolid()) {
                        return;
                    }
                }
            } else if (world.isMaterialInBB(bb, Material.LEAVES)) { // pass through leaves
                return;
            } else if (world.isMaterialInBB(bb, Material.GLASS)) { // edge case glass, we should dismount
                remove();
            } else {
                BlockPos bp = new BlockPos(passenger).down(Math.round((float) bb.minY));
                if (!world.getBlockState(bp).isSolid()) {
                    return;
                }
            }
            remove();
        }
    }

    protected void applyYawToEntity(Entity entityToUpdate) {
        entityToUpdate.setRenderYawOffset(rotationYaw);
        float yaw = MathHelper.wrapDegrees(entityToUpdate.rotationYaw - rotationYaw);
        float yawClamp = MathHelper.clamp(yaw, -Consts.HEAD_TURN_ANGLE, Consts.HEAD_TURN_ANGLE);
        entityToUpdate.prevRotationYaw += yawClamp - yaw;
        entityToUpdate.rotationYaw += yawClamp - yaw;
        entityToUpdate.setRotationYawHead(entityToUpdate.rotationYaw);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void applyOrientationToEntity(@Nonnull Entity entityToUpdate) {
        applyYawToEntity(entityToUpdate);
    }

    @Override
    protected boolean canFitPassenger(@Nonnull Entity passenger) {
        return getPassengers().isEmpty();
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void setVelocity(double x, double y, double z) {
        velocityX = x;
        velocityY = y;
        velocityZ = z;
        setMotion(velocityX, velocityY, velocityZ);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void setPositionAndRotationDirect(double x, double y, double z, float yaw, float pitch, int posRotationIncrements, boolean teleport) {
        gliderX = x;
        gliderY = y;
        gliderZ = z;
        gliderYaw = yaw;
        gliderPitch = pitch;
        turnProgress = 10;
        setMotion(velocityX, velocityY, velocityZ);
    }

    @SuppressWarnings("unused")
    public enum Color {
        BLACK("black"),
        BLUE("blue"),
        BROWN("brown"),
        CYAN("cyan"),
        GRAY("gray"),
        GREEN("green"),
        LIGHT_BLUE("light_blue"),
        LIGHT_GRAY("light_gray"),
        LIME("lime"),
        MAGENTA("magenta"),
        ORANGE("orange"),
        PINK("pink"),
        PURPLE("purple"),
        RED("red"),
        WHITE("white"),
        YELLOW("yellow"),
        CAMO("camo"),
        RAINBOW("rainbow");

        private final String name;

        Color(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String toString() {
            return name;
        }

        public static PoweredParagliderEntity.Color byID(int id) {
            PoweredParagliderEntity.Color[] gliderColors = values();
            if (id < 0 || id >= gliderColors.length) {
                id = 0;
            }
            return gliderColors[id];
        }

        public static PoweredParagliderEntity.Color getColorFromString(String colorStr) {
            PoweredParagliderEntity.Color[] gliderColors = values();

            for (Color color : gliderColors) {
                if (color.getName().equals(colorStr)) {
                    return color;
                }
            }
            return gliderColors[0];
        }
    }

}
