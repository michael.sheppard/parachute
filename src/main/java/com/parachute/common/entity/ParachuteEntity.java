/*
 * ParachuteEntity.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.common.entity;

import com.parachute.common.Consts;
import com.parachute.common.event.ConfigHandler;
import com.parachute.common.ModObjects;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.*;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;

import javax.annotation.Nonnull;
import java.util.List;

/*
 fixme:
    parachute position and movement is not updated on server for other players. The server knows where the parachute is
    but fails to inform other players, To the player riding the parachute everything seems normal, but other players
    only see the parachute/player floating to the ground.
 */

public class ParachuteEntity extends Entity {
    private static final DataParameter<Integer> PARACHUTE_COLOR = EntityDataManager.createKey(ParachuteEntity.class, DataSerializers.VARINT);

    private static double deltaRotation;
    private static double curLavaDistance;
    private static boolean rideInWater;

    private boolean leftKeyDown;
    private boolean rightKeyDown;
    private boolean forwardKeyDown;
    private boolean backKeyDown;
    private int turnProgress;

    public static boolean leftTurn;
    public static boolean rightTurn;
    public static double forwardMotion;

    private double chuteX;
    private double chuteY;
    private double chuteZ;
    private double chuteYaw;
    private double chutePitch;

    @OnlyIn(Dist.CLIENT)
    private double velocityX;
    @OnlyIn(Dist.CLIENT)
    private double velocityY;
    @OnlyIn(Dist.CLIENT)
    private double velocityZ;


    public static ParachuteEntity create(EntityType<? extends ParachuteEntity> type, World world) {
        return new ParachuteEntity(type, world);
    }

    public ParachuteEntity(EntityType<? extends ParachuteEntity> chute, World world) {
        super(chute, world);

        rideInWater = ConfigHandler.rideInWater;
        deltaRotation = 0.0;
        curLavaDistance = Consts.MIN_LAVA_DISTANCE;
        this.world = world;
        preventEntitySpawning = true;
        setSilent(false);
    }

    public ParachuteEntity(World world, double x, double y, double z) {
        this(ModObjects.PARACHUTE.get(), world);
        setPosition(x, y, z);
        setMotion(Vec3d.ZERO);
        prevPosX = x;
        prevPosY = y;
        prevPosZ = z;
    }

    @Override
    public boolean canRiderInteract() {
        return true;
    }

    @Override
    public AxisAlignedBB getCollisionBox(Entity entity) {
        if (entity != getControllingPassenger() && entity.getRidingEntity() != this) {
            return entity.getBoundingBox();
        }
        return null;
    }

    @Override
    public AxisAlignedBB getCollisionBoundingBox() {
        return getBoundingBox();
    }

    @Override
    public void applyEntityCollision(@Nonnull Entity entity) {
        if (entity instanceof ParachuteEntity) {
            if (entity.getBoundingBox().minY < getBoundingBox().maxY) {
                super.applyEntityCollision(entity);
            }
        } else if (entity.getBoundingBox().minY <= getBoundingBox().minY) {
            super.applyEntityCollision(entity);
        }
    }

    // skydiver should hang when on the parachute and then
    // pick up legs when landing.
    @Override
    public boolean shouldRiderSit() {
        Entity skyDiver = getControllingPassenger();
        if (skyDiver != null) {
            return shouldRiderPickupLegs(new BlockPos(skyDiver));
        }
        return false;
    }

    // ray tracing for shouldRiderSit method
    private boolean shouldRiderPickupLegs(BlockPos bp) {
        Vec3d v1 = new Vec3d(bp.getX(), bp.getY(), bp.getZ());
        Vec3d v2 = new Vec3d(bp.getX(), bp.down(5).getY(), bp.getZ());
        RayTraceResult traceResult = world.rayTraceBlocks(new RayTraceContext(v1, v2, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.ANY, this));
        if (traceResult.getType() == RayTraceResult.Type.BLOCK) {
            BlockPos blockpos = new BlockPos(traceResult.getHitVec().x, traceResult.getHitVec().y, traceResult.getHitVec().z);
            return !world.getBlockState(blockpos).isAir(world, blockpos);
        }
        return false;
    }

    @Nonnull
    @Override
    public Direction getAdjustedHorizontalFacing() {
        return getHorizontalFacing().rotateY();
    }

    @Override
    public boolean canPassengerSteer() {
        return true;
    }

    @Override
    @Nonnull
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public Entity getControllingPassenger() {
        List<Entity> list = getPassengers();
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public boolean canBeRiddenInWater(Entity pilot) {
        return rideInWater;
    }

    @Override
    protected boolean canBeRidden(Entity entity) {
        return true;
    }

    @Override
    public double getMountedYOffset() {
        return -Consts.CHUTE_OFFSET;
    }

    @Override
    public boolean canBeCollidedWith() {
        return isAlive();
    }

    @Override
    protected void readAdditional(@Nonnull CompoundNBT compoundNBT) {
    }

    @Override
    protected void writeAdditional(@Nonnull CompoundNBT compoundNBT) {
    }

    @Override
    protected void addPassenger(Entity passenger) {
        super.addPassenger(passenger);
        setPositionAndRotation(getPosV().x, getPosV().y, getPosV().z, rotationYaw, rotationPitch);
    }

    // updateInputs is called by ParachuteInputEvent class
    @OnlyIn(Dist.CLIENT)
    public void updateInputs(boolean leftKeyDown, boolean rightKeyDown, boolean forwardKeyDown, boolean backKeyDown) {
        this.leftKeyDown = leftKeyDown;
        this.rightKeyDown = rightKeyDown;
        this.forwardKeyDown = forwardKeyDown;
        this.backKeyDown = backKeyDown;
    }

    private void controlParachute() {
        if (isBeingRidden()) {
            double motionFactor = 0.0f;

            if (forwardKeyDown) {
                motionFactor += Consts.FORWARD_MOMENTUM;
            }

            if (backKeyDown) {
                motionFactor -= Consts.BACK_MOMENTUM;
            }

            if (leftKeyDown) {
                deltaRotation -= Consts.ROTATION_MOMENTUM;
            }
            if (rightKeyDown) {
                deltaRotation += Consts.ROTATION_MOMENTUM;
            }

            // slight forward momentum while turning
            if (rightKeyDown != leftKeyDown && !forwardKeyDown && !backKeyDown) {
                motionFactor += Consts.SLIDE_MOMENTUM;
            }

            leftTurn = leftKeyDown;
            rightTurn = rightKeyDown;
            forwardMotion = motionFactor;

            rotationYaw += deltaRotation;

            double motionY = currentDescentRate();
            double motionX = MathHelper.sin((float) Math.toRadians(-rotationYaw)) * motionFactor;
            double motionZ = MathHelper.cos((float) Math.toRadians(rotationYaw)) * motionFactor;
            setMotion(getMotion().add(motionX, motionY, motionZ));

            if (isBadWeather() && rand.nextBoolean()) {
                applyTurbulence(world.isThundering());
            }
        }
    }

    @Override
    protected void registerData() {
        dataManager.register(PARACHUTE_COLOR, ParachuteEntity.Color.BLACK.ordinal());
    }

    public void setParachuteColor(ParachuteEntity.Color color) {
        dataManager.set(PARACHUTE_COLOR, color.ordinal());
    }

    public ParachuteEntity.Color getParachuteColor() {
        return ParachuteEntity.Color.byID(dataManager.get(PARACHUTE_COLOR));
    }

    @Override
    public boolean attackEntityFrom(@Nonnull DamageSource source, float amount) {
        return !isInvulnerableTo(source);
    }

    @Override
    public void tick() {
        Entity skyDiver = getControllingPassenger();
        // the player has pressed LSHIFT or been killed,
        // may be necessary for LSHIFT to kill the parachute
        if (skyDiver == null && !world.isRemote) { // server side
            remove();
            return;
        }

        generateContrails();

        prevPosX = getPosV().x;
        prevPosY = getPosV().y;
        prevPosZ = getPosV().z;

        super.tick();
        setPacketCoordinates(getPosV().x, getPosV().y, getPosV().z);
        if (canPassengerSteer()) {
            updateMotion();

            setPosition(getPosV().x, getPosV().y, getPosV().z);
            // apply momentum/decay
            Vec3d curMotion = getMotion();
            setMotion(curMotion.x * Consts.DECAY_MOMENTUM, curMotion.y * (curMotion.y < 0.0 ? 0.96 : 0.98), curMotion.z * Consts.DECAY_MOMENTUM);
            deltaRotation *= 0.9;

            if (world.isRemote) {
                controlParachute();
            }
            // move the PARACHUTE with the motion equations applied
            move(MoverType.SELF, getMotion());
        }

        // something bad happened, somehow the skydiver was killed.
        if (!world.isRemote && skyDiver != null && !skyDiver.isAlive()) { // server side
            remove();
        }

        doBlockCollisions();
    }

    public void updateMotion() {
        if (canPassengerSteer()) {
            turnProgress = 0;
            setPacketCoordinates(getPosV().x, getPosV().y, getPosV().z);
        }
        if (turnProgress > 0) {
            double dx = getPosV().x + (chuteX - getPosV().x) / (double) turnProgress;
            double dy = getPosV().y + (chuteY - getPosV().y) / (double) turnProgress;
            double dz = getPosV().z + (chuteZ - getPosV().z) / (double) turnProgress;
            double delta_r = MathHelper.wrapDegrees(chuteYaw - (double) rotationYaw);
            rotationYaw = (float) ((double) rotationYaw + delta_r / (double) turnProgress);
            rotationPitch = (float) ((double) rotationPitch + (chutePitch - (double) rotationPitch) / (double) turnProgress);
            --turnProgress;
            setPosition(dx, dy, dz);
            setRotation(rotationYaw, rotationPitch);
        }
    }

    // check for bad weather, if the biome can rain or snow check to see
    // if it is raining (or snowing) or thundering.
    private boolean isBadWeather() {
        BlockPos bp = new BlockPos(getPosV());
        Chunk chunk = world.getChunkAt(bp);
        boolean canSnow = chunk.getWorld().getBiome(bp).getPrecipitation() == Biome.RainType.SNOW;
        boolean canRain = chunk.getWorld().getBiome(bp).getPrecipitation() == Biome.RainType.RAIN;
        return (canRain || canSnow) && (world.isRaining() || world.isThundering());
    }

    // determines the descent rate based on whether or not
    // the space bar has been pressed. weather and lava affect
    // the final result.
    private double currentDescentRate(/*Entity skydiver*/) {
        double descentRate;

        descentRate = calcHeatSourceThermals();

        descentRate += (world.isRaining() ? 0.002 : world.isThundering() ? 0.004 : 0.0);

        if (getPosV().y >= Consts.MAX_ALTITUDE) {
            descentRate = Consts.DRIFT;
        }

        return -descentRate;
    }

    // the following three methods detect lava|fire below the player
    // at up to 'maxLavaDistance' blocks. added logic to detect campfire.
    private boolean isHeatSource(BlockPos bp) {
        Block block = world.getBlockState(bp).getBlock();
        if (block == Blocks.CAMPFIRE) {
            return true;
        }
        return world.isFlammableWithin(new AxisAlignedBB(bp).expand(0, 1, 0));
    }

    private Vec3d getPosV() {
        return getPositionVec();
    }

    private boolean isHeatSourceInRange(BlockPos bp) {
        Vec3d v1 = new Vec3d(getPosV().x, getPosV().y, getPosV().z);
        Vec3d v2 = new Vec3d(bp.getX(), bp.getY(), bp.getZ());
        RayTraceResult mop = world.rayTraceBlocks(new RayTraceContext(v1, v2, RayTraceContext.BlockMode.COLLIDER, RayTraceContext.FluidMode.ANY, this));
        if (mop.getType() == RayTraceResult.Type.BLOCK) {
            BlockPos blockpos = new BlockPos(mop.getHitVec().x, mop.getHitVec().y, mop.getHitVec().z);
            return isHeatSource(blockpos);
        }
        return false;
    }

    private double calcHeatSourceThermals() {
        double thermals = Consts.DRIFT;
        final double inc = 0.5;

        if (isHeatSourceInRange(new BlockPos(getPosV().x, getPosV().y - Consts.CHUTE_OFFSET - Consts.MAX_LAVA_DISTANCE, getPosV().z))) {
            curLavaDistance += inc;
            thermals = Consts.ASCEND;
            if (curLavaDistance >= Consts.MAX_LAVA_DISTANCE) {
                curLavaDistance = Consts.MIN_LAVA_DISTANCE;
                thermals = Consts.DRIFT;
            }
        } else {
            curLavaDistance = Consts.MIN_LAVA_DISTANCE;
        }
        return thermals;
    }

    // apply 'turbulence' in the form of a collision force
    private void applyTurbulence(boolean roughWeather) {
        double rmin = 0.1;
        double dPos = rmin + 0.9 * rand.nextDouble();

        if (dPos >= 0.20) {
            double rmax = roughWeather ? 0.8 : 0.5;
            rmax = (rand.nextInt(5) == 0) ? 1.2 : rmax;  // gusting
            double dX = rmin + (rmax - rmin) * rand.nextDouble();
            double dY = rmin + 0.2 * rand.nextDouble();
            double dZ = rmin + (rmax - rmin) * rand.nextDouble();

            dPos = MathHelper.sqrt(dPos);
            double dPosInv = 1.0 / dPos;

            dX /= dPos;
            dY /= dPos;
            dZ /= dPos;

            dPosInv = Math.min(dPosInv, 1.0);

            dX *= dPosInv;
            dY *= dPosInv;
            dZ *= dPosInv;

            dX *= 0.05;
            dY *= 0.05;
            dZ *= 0.05;

            if (rand.nextBoolean()) {
                addVelocity(-dX, -dY, -dZ);
            } else {
                addVelocity(dX, dY, dZ);
            }
        }
    }

    // generate condensation trails at the trailing edge
    // of the parachute. Yes, I know that most parachutes
    // don't generate contrails (no engines), but most worlds
    // aren't made of block with cubic cows either. If you
    // like, you can think of the trails as chemtrails.
    private void generateContrails() {
        Vec3d motionVec = getMotion();
        double velocity = Math.sqrt(motionVec.x * motionVec.x + motionVec.z * motionVec.z);
        double cosYaw = 2.25 * Math.cos(Math.toRadians(90.0 + rotationYaw));
        double sinYaw = 2.25 * Math.sin(Math.toRadians(90.0 + rotationYaw));

        for (int k = 0; (double) k < 1.0 + velocity; k++) {
            double sign = (rand.nextInt(2) * 2 - 1) * 0.7;
            double x = getPosV().x + (getPosV().x - prevPosX) + cosYaw * -0.45 + sinYaw * sign;
            double y = getPosV().y - 0.25;
            double z = getPosV().z + (getPosV().z - prevPosZ) + sinYaw * -0.45 - cosYaw * sign;

            if (velocity > 0.01) {
                world.addParticle(ParticleTypes.CLOUD, x, y, z, motionVec.x, motionVec.y, motionVec.z);
            }
        }
    }

    @Override
    public void updatePassenger(@Nonnull Entity passenger) {
        if (isPassenger(passenger)) {
            float offset = (float) (getMountedYOffset() + passenger.getYOffset());
            Vec3d vec3d = (new Vec3d(0.0, 0.0, 0.0)).rotateYaw(-rotationYaw * ((float) Math.PI / 180F) - ((float) Math.PI / 2F));
            passenger.setPosition(getPosV().x + vec3d.x, getPosV().y + (double) offset, getPosV().z + vec3d.z);
            passenger.rotationYaw += deltaRotation;
            passenger.setRotationYawHead(passenger.getRotationYawHead() + (float) deltaRotation);
            applyYawToEntity(passenger);

            // check for passenger collisions
            checkForPlayerCollisions(passenger);
        }
    }

    // check for player colliding with block below. stop riding if block is not air, water,
    // grass/vines, or snow/snow layers
    private void checkForPlayerCollisions(Entity passenger) {
        AxisAlignedBB bb = passenger.getBoundingBox();
        if (!world.isRemote && world.checkBlockCollision(bb)) {
            // if in water check dismount-in-water flag, also check for solid block below water
            if (world.isMaterialInBB(bb, Material.WATER)) {
                if (!rideInWater) {
                    remove();
                } else {
                    BlockPos bp = new BlockPos(passenger).down(Math.round((float) bb.minY));
                    if (!world.getBlockState(bp.down(Math.round((float) bb.minY))).isSolid()) {
                        return;
                    }
                }
            } else if (world.isMaterialInBB(bb, Material.LEAVES)) { // pass through leaves
                return;
            } else if (world.isMaterialInBB(bb, Material.GLASS)) { // edge case glass, we should dismount
                remove();
            } else {
                BlockPos bp = new BlockPos(passenger).down(Math.round((float) bb.minY));
                if (!world.getBlockState(bp).isSolid()) {
                    return;
                }
            }
            remove();
        }
    }

    protected void applyYawToEntity(Entity entityToUpdate) {
        entityToUpdate.setRenderYawOffset(rotationYaw);
        float yaw = MathHelper.wrapDegrees(entityToUpdate.rotationYaw - rotationYaw);
        float yawClamp = MathHelper.clamp(yaw, -Consts.HEAD_TURN_ANGLE, Consts.HEAD_TURN_ANGLE);
        entityToUpdate.prevRotationYaw += yawClamp - yaw;
        entityToUpdate.rotationYaw += yawClamp - yaw;
        entityToUpdate.setRotationYawHead(entityToUpdate.rotationYaw);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void applyOrientationToEntity(Entity entityToUpdate) {
        applyYawToEntity(entityToUpdate);
    }

    @Override
    protected boolean canFitPassenger(Entity passenger) {
        return getPassengers().isEmpty();
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void setVelocity(double x, double y, double z) {
        velocityX = x;
        velocityY = y;
        velocityZ = z;
        setMotion(velocityX, velocityY, velocityZ);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void setPositionAndRotationDirect(double x, double y, double z, float yaw, float pitch, int posRotationIncrements, boolean teleport) {
        chuteX = x;
        chuteY = y;
        chuteZ = z;
        chuteYaw = yaw;
        chutePitch = pitch;
        turnProgress = 10;
        setMotion(velocityX, velocityY, velocityZ);
    }

    @

            SuppressWarnings("unused")
    public enum Color {
        BLACK("black"),
        BLUE("blue"),
        BROWN("brown"),
        CYAN("cyan"),
        GRAY("gray"),
        GREEN("green"),
        LIGHT_BLUE("light_blue"),
        LIGHT_GRAY("light_gray"),
        LIME("lime"),
        MAGENTA("magenta"),
        ORANGE("orange"),
        PINK("pink"),
        PURPLE("purple"),
        RED("red"),
        WHITE("white"),
        YELLOW("yellow"),
        CAMO("camo"),
        RAINBOW("rainbow");

        private final String name;

        Color(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public String toString() {
            return name;
        }

        public static ParachuteEntity.Color byID(int id) {
            ParachuteEntity.Color[] chuteColors = values();
            if (id < 0 || id >= chuteColors.length) {
                id = 0;
            }
            return chuteColors[id];
        }

        public static ParachuteEntity.Color getColorFromString(String colorStr) {
            ParachuteEntity.Color[] chuteColors = values();

            for (Color color : chuteColors) {
                if (color.getName().equals(colorStr)) {
                    return color;
                }
            }
            return chuteColors[0];
        }
    }

}
