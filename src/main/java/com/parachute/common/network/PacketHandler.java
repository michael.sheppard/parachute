/*
 * PacketHandler.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.parachute.common.network;


import com.parachute.common.ParachuteMod;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

public final class PacketHandler {
    private static final String PROTOCOL_VERSION = Integer.toString(1);

    public static final SimpleChannel HANDLER = NetworkRegistry.ChannelBuilder
            .named(new ResourceLocation(ParachuteMod.MODID, "main_channel"))
            .clientAcceptedVersions(PROTOCOL_VERSION::equals)
            .serverAcceptedVersions(PROTOCOL_VERSION::equals)
            .networkProtocolVersion(() -> PROTOCOL_VERSION)
            .simpleChannel();

    public static void register() {
        HANDLER.registerMessage(0, ClientAADStateMessage.class, ClientAADStateMessage::encode, ClientAADStateMessage::decode, ClientAADStateMessage.Handler::handle);
        HANDLER.registerMessage(1, PoweredClientAADStateMessage.class, PoweredClientAADStateMessage::encode, PoweredClientAADStateMessage::decode, PoweredClientAADStateMessage.Handler::handle);
        HANDLER.registerMessage(2, ServerConfigMessage.class, ServerConfigMessage::encode, ServerConfigMessage::decode, ServerConfigMessage.Handler::handle);
        HANDLER.registerMessage(3, ClientFuelUpdateMessage.class, ClientFuelUpdateMessage::encode, ClientFuelUpdateMessage::decode, ClientFuelUpdateMessage.Handler::handle);
        HANDLER.registerMessage(4, ServerAscendModeMessage.class, ServerAscendModeMessage::encode, ServerAscendModeMessage::decode, ServerAscendModeMessage.Handler::handle);
    }
}