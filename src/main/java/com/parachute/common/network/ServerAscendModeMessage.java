/*
 * ServerAscendModeMessage.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.common.network;

import com.parachute.common.entity.PoweredParagliderEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class ServerAscendModeMessage {
    public boolean ascendMode;

    public ServerAscendModeMessage(boolean mode) {
        ascendMode = mode;
    }

    public static ServerAscendModeMessage decode(PacketBuffer buffer) {
        return new ServerAscendModeMessage(buffer.readBoolean());
    }

    public static void encode(ServerAscendModeMessage msg, PacketBuffer buffer) {
        buffer.writeBoolean(msg.ascendMode);
    }

    public static class Handler {
        public static void handle(final ServerAscendModeMessage pkt, Supplier<NetworkEvent.Context> ctx) {
            ctx.get().enqueueWork(() -> DistExecutor.runWhenOn(Dist.DEDICATED_SERVER, () -> () -> clientHandler(pkt)));
            ctx.get().setPacketHandled(true);
        }
    }

    public static void clientHandler(final ServerAscendModeMessage pkt) {
        PoweredParagliderEntity.setAscendMode(pkt.ascendMode);
    }
}
