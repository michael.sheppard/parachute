/*
 * PoweredClientAADStateMessage.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.common.network;

import com.parachute.common.item.PoweredParagliderItem;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class PoweredClientAADStateMessage {
    private boolean aadState;

    public PoweredClientAADStateMessage(boolean value) {
        aadState = value;
    }

    public static PoweredClientAADStateMessage decode(PacketBuffer buffer) {
        return new PoweredClientAADStateMessage(buffer.readBoolean());
    }

    public static void encode(PoweredClientAADStateMessage msg, PacketBuffer buffer) {
        buffer.writeBoolean(msg.aadState);
    }

    public static class Handler {
        public static void handle(final PoweredClientAADStateMessage pkt, Supplier<NetworkEvent.Context> ctx) {
            ctx.get().enqueueWork(() -> DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> clientHandler(pkt)));
            ctx.get().setPacketHandled(true);
        }
    }

    public static void clientHandler(final PoweredClientAADStateMessage pkt) {
        PoweredParagliderItem.setAADState(pkt.aadState);
    }
}
