/*
 * ServerConfigMessage.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.common.network;

import com.parachute.common.event.ConfigHandler;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class ServerConfigMessage {
    public boolean singleUse;
    public boolean thermals;
    public boolean rideInWater;

    public ServerConfigMessage(boolean singleUse, boolean thermals, boolean rideInWater) {
        this.singleUse = singleUse;
        this.thermals = thermals;
        this.rideInWater = rideInWater;
    }

    public static ServerConfigMessage decode(PacketBuffer pkt) {
        return new ServerConfigMessage(pkt.readBoolean(), pkt.readBoolean(), pkt.readBoolean());
    }

    public static void encode(ServerConfigMessage msg, PacketBuffer pkt) {
        pkt.writeBoolean(msg.singleUse);
        pkt.writeBoolean(msg.thermals);
        pkt.writeBoolean(msg.rideInWater);
    }

    public static class Handler {
        public static void handle(final ServerConfigMessage pkt, Supplier<NetworkEvent.Context> ctx) {
            ctx.get().enqueueWork(() -> DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> clientHandler(pkt)));
            ctx.get().setPacketHandled(true);
        }
    }

    @OnlyIn(Dist.CLIENT)
    public static void clientHandler(final ServerConfigMessage pkt) {
        ConfigHandler.singleUse = pkt.singleUse;
        ConfigHandler.allowFuelBurn = pkt.thermals;
        ConfigHandler.rideInWater = pkt.rideInWater;
    }

}
