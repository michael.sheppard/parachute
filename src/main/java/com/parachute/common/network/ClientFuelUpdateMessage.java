/*
 * ClientFuelUpdateMessage.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.common.network;

import com.parachute.common.item.PoweredParagliderItem;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientFuelUpdateMessage {
    private int fuelAmount;

    public ClientFuelUpdateMessage(int amount) {
        fuelAmount = amount;
    }

    public static ClientFuelUpdateMessage decode(PacketBuffer buffer) {
        return new ClientFuelUpdateMessage(buffer.readInt());
    }

    public static void encode(ClientFuelUpdateMessage msg, PacketBuffer buffer) {
        buffer.writeInt(msg.fuelAmount);
    }

    public static class Handler {
        public static void handle(final ClientFuelUpdateMessage pkt, Supplier<NetworkEvent.Context> ctx) {
            ctx.get().enqueueWork(() -> DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> clientHandler(pkt)));
            ctx.get().setPacketHandled(true);
        }
    }

    public static void clientHandler(final ClientFuelUpdateMessage pkt) {
        PoweredParagliderItem.setFuelAmount(pkt.fuelAmount);
    }
}
