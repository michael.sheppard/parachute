/*
 * PoweredParagliderRepairEvent.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.common.event;

import com.parachute.common.item.PoweredParagliderItem;
import com.parachute.common.network.ClientFuelUpdateMessage;
import com.parachute.common.network.PacketHandler;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraftforge.event.AnvilUpdateEvent;
import net.minecraftforge.event.entity.player.AnvilRepairEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.network.PacketDistributor;


@SuppressWarnings("unused")
public class PoweredParagliderRepairEvent {
    // fuel.capacity == ModObjects.PARAGLIDER_MAX_DAMAGE == 1280
    // the fuel 'tank' capacity is 1280 units. each unit is 1/20
    // of a redstone 'unit'. The 'tank' requires 64 redstones
    // to fill.
    private static final int MAX_REDSTONE_FILL = 64;
    private static final int REDSTONE_UNIT_DIVISOR = 20;

    // calculate the redstones needed to repair/refuel the paraglider
    @SubscribeEvent
    public void onParagliderAnvilRepairEvent(AnvilRepairEvent event) {
        if (!event.getPlayer().world.isRemote) {
            ItemStack ingredientInput = event.getIngredientInput();
            if (ingredientInput.getItem().equals(Items.REDSTONE) && event.getItemInput().getItem() instanceof PoweredParagliderItem) {
                int damage = event.getItemInput().getDamage();
                int neededRedstones = Math.round(((float)damage / (float)REDSTONE_UNIT_DIVISOR));
                int usedRedstones = Math.min(Math.min(ingredientInput.getCount(), neededRedstones), MAX_REDSTONE_FILL);

                event.getItemResult().setDamage(Math.max(damage - (usedRedstones * REDSTONE_UNIT_DIVISOR), 0));
                // fuelAmount is checked for overflow in addFuelAmount
                PoweredParagliderItem.addFuelAmount(usedRedstones * REDSTONE_UNIT_DIVISOR);
                event.setBreakChance(0.01f); // 1% break chance

                PacketHandler.HANDLER.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity) event.getPlayer()),
                        new ClientFuelUpdateMessage(usedRedstones * REDSTONE_UNIT_DIVISOR));
            }
        }
    }

    // set the output paraglider and repair costs, setting the cost will avoid the 'too expensive' bug
    @SubscribeEvent
    public void onParagliderAnvilUpdateEvent(AnvilUpdateEvent event) {
        if (event.getRight().getItem().equals(Items.REDSTONE) && event.getLeft().getItem() instanceof PoweredParagliderItem) {
            int damage = event.getLeft().getDamage();
            int neededRedstones = Math.round(((float)damage / (float)REDSTONE_UNIT_DIVISOR));
            int usedRedstones = Math.min(Math.min(event.getRight().getCount(), neededRedstones), MAX_REDSTONE_FILL);

            // copy the input paraglider and set as the output paraglider
            ItemStack output = event.getLeft().copy();
            event.setOutput(output);

            // cost is 5 experience points
            event.getLeft().setRepairCost(5);
            event.setCost(5);
            // remove 'usedRedstones' number of redstones from the right input
            event.setMaterialCost(usedRedstones);
        }
    }
}
