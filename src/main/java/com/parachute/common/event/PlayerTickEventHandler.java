/*
 * PlayerTickEventHandler.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.parachute.common.event;

import com.parachute.common.ModObjects;
import com.parachute.common.ParachuteMod;
import com.parachute.common.item.ParachutePackItem;
import com.parachute.common.entity.ParachuteEntity;
import com.parachute.common.entity.PoweredParagliderEntity;
import com.parachute.common.item.ParachuteItem;
import com.parachute.common.item.PoweredParagliderItem;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.gui.ForgeIngameGui;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.registries.ForgeRegistries;

@SuppressWarnings("unused")
public class PlayerTickEventHandler {

    private static boolean displayArmorBar;

    @SubscribeEvent
    public void onTick(TickEvent.PlayerTickEvent event) {
        if (event.phase.equals(TickEvent.Phase.START)) {
            autoActivateDevice(event.player);
            autoActivateDevicePowered(event.player);
            togglePlayerParachutePack(event.player);
            armorBarRenderingHandler(event.player);
        }
    }

    // Check the players currently held item and if it is a
    // PARACHUTE_ITEM set a PARACHUTEPACK_ITEM in the chestplate armor slot.
    // Remove the PARACHUTEPACK_ITEM if the player is no longer holding the PARACHUTE_ITEM
    // as long as the player is not on the PARACHUTE. If there is already an
    // armor item in the armor slot do nothing.
    private void togglePlayerParachutePack(PlayerEntity player) {
        if (player != null) {
            ItemStack armor = player.getItemStackFromSlot(EquipmentSlotType.CHEST);
            boolean deployed = player.getRidingEntity() instanceof ParachuteEntity;
            boolean isParachuteHeld = isParachuteHeldItem(player) || isPoweredParagliderHeldItem(player);

            if (!deployed && armor.getItem() instanceof ParachutePackItem && !isParachuteHeld) {
                player.inventory.armorInventory.set(EquipmentSlotType.CHEST.getIndex(), ItemStack.EMPTY);
            } else if (isParachuteHeld && armor.isEmpty()) {
                Item pack = ForgeRegistries.ITEMS.getValue(new ResourceLocation(ParachuteMod.MODID, ModObjects.PACK_NAME));
                player.inventory.armorInventory.set(EquipmentSlotType.CHEST.getIndex(), new ItemStack(pack));
            }
        }
    }

    private boolean isParachuteHeldItem(PlayerEntity player) {
        Iterable<ItemStack> items = player.getHeldEquipment();
        for (ItemStack itemStack : items) {
            if (itemStack.getItem() instanceof ParachuteItem) {
                return true;
            }
        }
        return false;
    }

    private boolean isPoweredParagliderHeldItem(PlayerEntity player) {
        Iterable<ItemStack> items = player.getHeldEquipment();
        for (ItemStack itemStack : items) {
            if (itemStack.getItem() instanceof PoweredParagliderItem) {
                return true;
            }
        }
        return false;
    }

    // do not display the armorbar if the PARACHUTE is selected in the hot bar
    // and no other armor is being worn
    private void armorBarRenderingHandler(PlayerEntity player) {
        if (player != null) {
            for (EquipmentSlotType slot : EquipmentSlotType.values()) {
                if (player.getItemStackFromSlot(slot).getItem() instanceof ArmorItem) {
                    displayArmorBar = !(player.getItemStackFromSlot(slot).getItem() instanceof ParachutePackItem);
                }
            }
            if (player.world.isRemote) {
                ForgeIngameGui.renderArmor = displayArmorBar;
            }
        }
    }

    // Handles the Automatic Activation Device, if the AAD is active
    // and the player is actually wearing the parachute deploy after
    // AAD_FALL_DISTANCE is reached.
    private void autoActivateDevice(PlayerEntity player) {
        boolean aadState = ParachuteItem.getAADState();

        if (aadState && !(player.getRidingEntity() instanceof ParachuteEntity)) {
            ItemStack heldItem = null;
            Iterable<ItemStack> heldEquipment = player.getHeldEquipment();
            for (ItemStack itemStack : heldEquipment) {
                if (itemStack != null && itemStack.getItem() instanceof ParachuteItem) {
                    heldItem = itemStack;
                }
            }
            final double AAD_FALL_DISTANCE = 5.0;
            if (player.fallDistance > AAD_FALL_DISTANCE) {
                if (heldItem != null && heldItem.getItem() instanceof ParachuteItem) {
                    ((ParachuteItem) heldItem.getItem()).deployParachute(player.world, player, heldItem);
                }
            }
        }
    }

    // Handles the Automatic Activation Device, if the AAD is active
    // and the player is actually wearing the powered_paraglider deploy after
    // AAD_FALL_DISTANCE is reached.
    private void autoActivateDevicePowered(PlayerEntity player) {
        boolean aadState = PoweredParagliderItem.getAADState();

        if (aadState && !(player.getRidingEntity() instanceof PoweredParagliderEntity)) {
            ItemStack heldItem = null;
            Iterable<ItemStack> heldEquipment = player.getHeldEquipment();
            for (ItemStack itemStack : heldEquipment) {
                if (itemStack != null && itemStack.getItem() instanceof PoweredParagliderItem) {
                    heldItem = itemStack;
                }
            }
            final double AAD_FALL_DISTANCE = 5.0;
            if (player.fallDistance > AAD_FALL_DISTANCE) {
                if (heldItem != null && heldItem.getItem() instanceof PoweredParagliderItem) {
                    ((PoweredParagliderItem) heldItem.getItem()).deployPoweredParaglider(player.world, player, heldItem);
                }
            }
        }
    }
}
