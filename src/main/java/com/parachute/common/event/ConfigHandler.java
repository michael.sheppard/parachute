/*
 * ConfigHandler.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.parachute.common.event;


import com.parachute.common.ParachuteMod;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ConfigFileTypeHandler;
import net.minecraftforge.fml.config.ModConfig;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Path;


@Mod.EventBusSubscriber(modid = ParachuteMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
@SuppressWarnings("unused")
public class ConfigHandler {
    public static boolean singleUse;
    public static boolean allowFuelBurn;
    public static boolean rideInWater;
    public static boolean showRollEffect;

    private static ConfigFileTypeHandler modConfig;

    public static final ForgeConfigSpec commonSpec;
    public static final CommonConfig COMMON_CONFIG;
    private static Path commonConfigPath;

    static {
        final Pair<CommonConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(CommonConfig::new);
        commonSpec = specPair.getRight();
        COMMON_CONFIG = specPair.getLeft();
    }

    @SubscribeEvent
    public static void onModConfigEvent(final ModConfig.ModConfigEvent event) {
        // save the config object
        modConfig = event.getConfig().getHandler();
        if (event.getConfig().getSpec() == commonSpec) {
            // save the common config  path
            commonConfigPath = event.getConfig().getFullPath();
            bakeCommonConfig();
        }
    }

    public static void reLoadCommonConfig() {
        modConfig.reader(commonConfigPath);
        bakeCommonConfig();
    }

    public static void bakeCommonConfig() {
        singleUse = COMMON_CONFIG.parachuteSingleUse.get();
        allowFuelBurn = COMMON_CONFIG.fuelBurn.get();
        rideInWater = COMMON_CONFIG.rideInWater.get();
        showRollEffect = COMMON_CONFIG.showRollEffect.get();
    }

    public static class CommonConfig {
        public ForgeConfigSpec.BooleanValue parachuteSingleUse;
        public ForgeConfigSpec.BooleanValue fuelBurn;
        public ForgeConfigSpec.BooleanValue rideInWater;
        public ForgeConfigSpec.BooleanValue showRollEffect;

        public CommonConfig(ForgeConfigSpec.Builder builder) {
            builder.comment("CommonConfig Config")
                    .push("CommonConfig");

            parachuteSingleUse = builder
                    .comment("set to true for parachute single use [default: false]")
                    .translation("config.parachutemod.parachuteSingleUse")
                    .define("singleUse", false);

            fuelBurn = builder
                    .comment("enable fuel burn rise by pressing the space bar [default: true]",
                            "only applies to the powered parachute")
                    .translation("config.parachutemod.fuelBurn")
                    .define("allowFulBurn", true);

            rideInWater = builder
                    .comment("if true, allow riding in water [default: true]")
                    .translation("config.parachutemod.rideInWater")
                    .define("rideInWater", true);

            showRollEffect = builder
                    .comment("show roll effect when riding parachute")
                    .translation("config.parachutemod.showRollEffect")
                    .define("showRollEffect", true);

            builder.pop();
        }
    }

}
