/*
 * PlayerLoginHandler.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.common.event;

import com.parachute.common.network.PacketHandler;
import com.parachute.common.network.ServerConfigMessage;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.network.NetworkDirection;

import java.util.function.Predicate;

@SuppressWarnings("unused")
public class PlayerLoginHandler {
    public PlayerLoginHandler() {
    }

    @SubscribeEvent
    public void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
        PlayerManager.getInstance().Players.add(new PlayerInfo(event.getPlayer().getDisplayName().toString()));
        boolean onDedicatedServer = ((ServerPlayerEntity) event.getPlayer()).server.isDedicatedServer();

        if (!event.getPlayer().world.isRemote) { // server sends config to client
            if (onDedicatedServer) { // on the dedicated server
                ServerConfigMessage MSG = new ServerConfigMessage(ConfigHandler.singleUse, ConfigHandler.allowFuelBurn, ConfigHandler.rideInWater);
                PacketHandler.HANDLER.sendTo(MSG, ((ServerPlayerEntity) event.getPlayer()).connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
            } else { // on the integrated server
                // hack: reload the common/server config from disk
                ConfigHandler.reLoadCommonConfig();
            }
        }
    }

    @SubscribeEvent
    public void onPlayerLogout(PlayerEvent.PlayerLoggedOutEvent event) {
        Predicate<PlayerInfo> player = p -> event.getPlayer().getDisplayName().toString().equals(p.getName());
        PlayerManager.getInstance().Players.removeIf(player);
    }

    public static class PlayerInfo {
        private String name;

        public PlayerInfo(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}
