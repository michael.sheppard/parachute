/*
 * PoweredParagliderItem.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.parachute.common.item;

import com.parachute.client.sound.ParachuteFlyingSound;
import com.parachute.common.Consts;
import com.parachute.common.ModObjects;
import com.parachute.common.ParachuteMod;
import com.parachute.common.entity.PoweredParagliderEntity;
import com.parachute.common.network.ClientFuelUpdateMessage;
import com.parachute.common.network.PacketHandler;
import com.parachute.common.network.PoweredClientAADStateMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.UseAction;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.item.Item;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public class PoweredParagliderItem extends Item {
    private static boolean aadState = true;
    private static int fuelAmount = Consts.PARAGLIDER_MAX_DAMAGE;
    private final PoweredParagliderEntity.Color color;

    public PoweredParagliderItem(PoweredParagliderEntity.Color color_val, Properties props) {
        super(props);
        color = color_val;
    }

    @Nonnull
    @Override
    public ActionResult<ItemStack> onItemRightClick(@Nonnull World world, PlayerEntity playerEntity, @Nonnull Hand hand) {
        ItemStack itemstack = playerEntity.getHeldItem(hand);
        if (isFalling(playerEntity) && playerEntity.getRidingEntity() == null) {
            return deployPoweredParaglider(world, playerEntity, itemstack);
        } else { // toggle the AAD state
            toggleAAD(itemstack, world, playerEntity);
            return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
        }
    }

    public ActionResult<ItemStack> deployPoweredParaglider(World world, PlayerEntity playerEntity, ItemStack itemstack) {
        PoweredParagliderEntity glider = new PoweredParagliderEntity(world, playerEntity.getPositionVec().x, playerEntity.getPositionVec().y + Consts.GLIDER_OFFSET, playerEntity.getPositionVec().z);
        glider.setPoweredParagliderColor(color);
        glider.rotationYaw = playerEntity.rotationYaw; // set paraglider facing player direction
        fuelAmount = (itemstack.getMaxDamage() - itemstack.getDamage());
        glider.setFuelAmount(fuelAmount);
        glider.setItemstack(itemstack);

        // check for collisions
        if (!world.hasNoCollisions(playerEntity, playerEntity.getBoundingBox().grow(-0.1))) {
            return new ActionResult<>(ActionResultType.FAIL, itemstack);
        }

        final float volume = 1.0F;
        SoundEvent sound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(ParachuteMod.MODID, ModObjects.CHUTE_OPEN_SOUND));
        if (sound != null) {
            glider.playSound(sound, volume, audio_pitch());
        }

        if (world.isRemote) { // client side
            playFlyingSound((ClientPlayerEntity)playerEntity);
        } else { // server side
            world.addEntity(glider);
            playerEntity.startRiding(glider);
            PacketHandler.HANDLER.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)playerEntity), new ClientFuelUpdateMessage(fuelAmount));
        }

        playerEntity.addStat(Stats.ITEM_USED.get(this)); // update powered paraglider deployed statistics

        Iterable<ItemStack> heldEquipment = playerEntity.getHeldEquipment();
        for (ItemStack itemStack : heldEquipment) {
            if (itemStack != null && itemStack.getItem() instanceof PoweredParagliderItem) {
                itemstack = itemStack;
            }
        }
        return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void addInformation(@Nonnull ItemStack stack, @Nullable World worldIn, @Nonnull List<ITextComponent> tooltip, @Nonnull ITooltipFlag tooltipFlag) {
        super.addInformation(stack, worldIn, tooltip, tooltipFlag);

        fuelAmount = (stack.getMaxDamage() - stack.getDamage());
        double fuel = getFuelAmountPercent();
        TextFormatting format = fuel < 21.0 ? TextFormatting.RED : fuel < 51.0 ? TextFormatting.YELLOW : TextFormatting.GREEN;
        String fuelStr = String.format("%3.1f%%", fuel);
        tooltip.add(new StringTextComponent("Fuel Available: " + fuelStr).applyTextStyle(TextFormatting.BOLD).applyTextStyle(format));

        int damage = stack.getDamage();
        int maxDamage = stack.getMaxDamage();
        TextFormatting fmt = damage > (maxDamage * 0.8) ? TextFormatting.RED : damage > (maxDamage * 0.5) ? TextFormatting.YELLOW : TextFormatting.GREEN;
        String damageStr = String.format("%2d", damage);
        tooltip.add(new StringTextComponent("Damage: " + damageStr).applyTextStyle(TextFormatting.BOLD).applyTextStyle(fmt));
    }

    private void toggleAAD(ItemStack itemstack, World world, PlayerEntity playerEntity) {
        if (playerEntity != null) {
            boolean curAADState = aadState;
            if (!world.isRemote) { // server side
                curAADState = !curAADState;
                aadState = curAADState;
                String aadString = new TranslationTextComponent(curAADState ? "aad.active" : "aad.inactive").getString();
                itemstack.setDisplayName(new StringTextComponent(getDisplayName(itemstack).getString() + aadString));
                PacketHandler.HANDLER.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)playerEntity), new PoweredClientAADStateMessage(curAADState));
            } else { // client side
                world.playSound(playerEntity, new BlockPos(playerEntity.getPositionVec().x, playerEntity.getPositionVec().y + Consts.GLIDER_OFFSET, playerEntity.getPositionVec().z),
                        SoundEvents.UI_BUTTON_CLICK, SoundCategory.MASTER, 1.0f, 1.0f);
            }
        }
    }

    private boolean isFalling(PlayerEntity player) {
        return (player.fallDistance > 0.0F && !player.onGround && !player.isOnLadder());
    }

    private float audio_pitch() {
        return 1.0F / (random.nextFloat() * 0.4F + 0.8F);
    }

    @Override
    @Nonnull
    public UseAction getUseAction(@Nonnull ItemStack stack) {
        return UseAction.NONE;
    }

    @Override
    public boolean getIsRepairable(@Nonnull ItemStack toRepair, ItemStack repair) {
        return Items.REDSTONE.equals(repair.getItem());
    }

    @OnlyIn(Dist.CLIENT)
    private void playFlyingSound(ClientPlayerEntity playerEntity) {
        Minecraft.getInstance().getSoundHandler().play(new ParachuteFlyingSound(playerEntity));
    }

    public static boolean getAADState() {
        return aadState;
    }

    public static void setAADState(boolean state) {
        aadState = state;
    }

    public static double getFuelAmountPercent() {
        return fuelAmount * (100.0 / Consts.PARAGLIDER_MAX_DAMAGE);
    }

    public static void setFuelAmount(int amount) {
        fuelAmount = Math.max(0, Math.min(amount, Consts.PARAGLIDER_MAX_DAMAGE));
    }

    public static void addFuelAmount(int amount) {
        fuelAmount += amount;
        fuelAmount = Math.max(0, Math.min(fuelAmount, Consts.PARAGLIDER_MAX_DAMAGE));
    }

}
