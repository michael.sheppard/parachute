/*
 * ParachuteItem.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.parachute.common.item;

import com.parachute.client.sound.ParachuteFlyingSound;
import com.parachute.common.Consts;
import com.parachute.common.event.ConfigHandler;
import com.parachute.common.ParachuteMod;
import com.parachute.common.entity.ParachuteEntity;
import com.parachute.common.network.ClientAADStateMessage;
import com.parachute.common.network.PacketHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.item.Item;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.registries.ForgeRegistries;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public class ParachuteItem extends Item {
    private static boolean aadState = true;
    private final ParachuteEntity.Color color;

    public ParachuteItem(ParachuteEntity.Color color_val, Properties props) {
        super(props);
        color = color_val;
    }

    @Nonnull
    public ActionResult<ItemStack> onItemRightClick(@Nonnull World world, PlayerEntity playerEntity, @Nonnull Hand hand) {
        ItemStack itemstack = playerEntity.getHeldItem(hand);
        if (isFalling(playerEntity) && playerEntity.getRidingEntity() == null) {
            return deployParachute(world, playerEntity, itemstack);
        } else { // toggle the AAD state
            toggleAAD(itemstack, world, playerEntity);
            return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
        }
    }

    public ActionResult<ItemStack> deployParachute(World world, PlayerEntity playerEntity, ItemStack itemstack) {
        ParachuteEntity chute = new ParachuteEntity(world, playerEntity.getPositionVec().x, playerEntity.getPositionVec().y + Consts.CHUTE_OFFSET, playerEntity.getPositionVec().z);
        chute.setParachuteColor(color);
        chute.rotationYaw = playerEntity.rotationYaw; // set parachute facing player direction

        // check for collisions
        if (!world.hasNoCollisions(playerEntity, playerEntity.getBoundingBox().grow(-0.1))) {
            return new ActionResult<>(ActionResultType.FAIL, itemstack);
        }

        final float volume = 1.0F;
        SoundEvent sound = ForgeRegistries.SOUND_EVENTS.getValue(new ResourceLocation(ParachuteMod.MODID, "chuteopen"));
        if (sound != null) {
            chute.playSound(sound, volume, audio_pitch());
        }

        if (world.isRemote) { // client side
            playFlyingSound((ClientPlayerEntity)playerEntity);
        } else { // server side
            world.addEntity(chute);
            playerEntity.startRiding(chute);
        }

        playerEntity.addStat(Stats.ITEM_USED.get(this)); // update parachute deployed statistics

        Iterable<ItemStack> heldEquipment = playerEntity.getHeldEquipment();
        for (ItemStack itemStack : heldEquipment) {
            if (itemStack != null && itemStack.getItem() instanceof ParachuteItem) {
                itemstack = itemStack;
            }
        }
        if (!playerEntity.abilities.isCreativeMode || itemstack.isDamageable()) {
            if (ConfigHandler.singleUse) {
                itemstack.shrink(1);
            } else {
                int damage = itemstack.getDamage();
                itemstack.setDamage(damage + 1);
                if (damage >= itemstack.getMaxDamage()) {
                    itemstack.shrink(1);
                }
            }
        }
        return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void addInformation(@Nonnull ItemStack stack, @Nullable World worldIn, @Nonnull List<ITextComponent> tooltip, @Nonnull ITooltipFlag flagIn) {
        super.addInformation(stack, worldIn, tooltip, flagIn);

        int damage = stack.getDamage();
        int maxDamage = stack.getMaxDamage();
        TextFormatting fmt = damage > (maxDamage * 0.8) ? TextFormatting.RED : damage > (maxDamage * 0.5) ? TextFormatting.YELLOW : TextFormatting.GREEN;
        String damageStr = String.format("%2d", damage);
        tooltip.add(new StringTextComponent("Damage: " + damageStr).applyTextStyle(TextFormatting.BOLD).applyTextStyle(fmt));
    }

    private void toggleAAD(ItemStack itemstack, World world, PlayerEntity playerEntity) {
        if (playerEntity != null) {
            boolean curAADState = aadState;
            if (!world.isRemote) { // server side
                curAADState = !curAADState;
                aadState = curAADState;
                String aadString = new TranslationTextComponent(curAADState ? "aad.active" : "aad.inactive").getString();
                itemstack.setDisplayName(new StringTextComponent(getDisplayName(itemstack).getString() + aadString));
                PacketHandler.HANDLER.sendTo(new ClientAADStateMessage(curAADState), ((ServerPlayerEntity) playerEntity).connection.netManager, NetworkDirection.PLAY_TO_CLIENT);
            } else { // client side
                world.playSound(playerEntity, new BlockPos(playerEntity.getPositionVec().x, playerEntity.getPositionVec().y + Consts.CHUTE_OFFSET, playerEntity.getPositionVec().z),
                        SoundEvents.UI_BUTTON_CLICK, SoundCategory.MASTER, 1.0f, 1.0f);
            }
        }
    }

    private boolean isFalling(PlayerEntity player) {
        return (player.fallDistance > 0.0F && !player.onGround && !player.isOnLadder());
    }

    private float audio_pitch() {
        return 1.0F / (random.nextFloat() * 0.4F + 0.8F);
    }

    @Override
    public boolean getIsRepairable(@Nonnull ItemStack toRepair, ItemStack repair) {
        return Items.STRING.equals(repair.getItem());
    }

    @OnlyIn(Dist.CLIENT)
    private void playFlyingSound(ClientPlayerEntity playerEntity) {
        Minecraft.getInstance().getSoundHandler().play(new ParachuteFlyingSound(playerEntity));
    }

    public static boolean getAADState() {
        return aadState;
    }

    public static void setAADState(boolean state) {
        aadState = state;
    }
}
