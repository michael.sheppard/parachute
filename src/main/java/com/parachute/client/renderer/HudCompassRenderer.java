/*
 * HudCompassRenderer.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.client.renderer;

import com.mojang.blaze3d.systems.RenderSystem;
import com.parachute.common.*;
import com.parachute.common.entity.ParachuteEntity;
import com.parachute.common.entity.PoweredParagliderEntity;
import com.parachute.common.item.ParachuteItem;
import com.parachute.common.item.PoweredParagliderItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@OnlyIn(Dist.CLIENT)
@SuppressWarnings("unused")
public class HudCompassRenderer extends AbstractGui {
    protected static final ResourceLocation COMPASS_TEXTURE = new ResourceLocation(ParachuteMod.MODID + ":" + "textures/gui/hud-compass.png");
    protected static final ResourceLocation HOME_TEXTURE = new ResourceLocation(ParachuteMod.MODID + ":" + "textures/gui/hud-home.png");
    protected static final ResourceLocation RETICULE_RED_TEXTURE = new ResourceLocation(ParachuteMod.MODID + ":" + "textures/gui/hud-reticule_red.png");
    protected static final ResourceLocation RETICULE_GREEN_TEXTURE = new ResourceLocation(ParachuteMod.MODID + ":" + "textures/gui/hud-reticule_green.png");
    protected static final ResourceLocation RETICULE_YELLOW_TEXTURE = new ResourceLocation(ParachuteMod.MODID + ":" + "textures/gui/hud-reticule_yellow.png");
    protected static final ResourceLocation BACKGROUND_TEXTURE = new ResourceLocation(ParachuteMod.MODID + ":" + "textures/gui/hud-background.png");
    protected static final ResourceLocation NIGHT_TEXTURE = new ResourceLocation(ParachuteMod.MODID + ":" + "textures/gui/hud-night.png");

    private static final int MOON_RISE = 12600;
    private static final int SUN_RISE = 22900;
    private static final int MAX_TICKS = 24000;

    private static final int COLOR_RED = 0xffff0000;
    private static final int COLOR_GREEN = 0xff00ff00;
    private static final int COLOR_YELLOW = 0xffffff00;

    private static final Minecraft MINECRAFT = Minecraft.getInstance();

    public static double altitude;
    private static boolean isVisible = true;

    private static int count = 0;

    private static final int HUD_WIDTH = 256;
    private static final int HUD_HEIGHT = 256;
    private static final int Y_PADDING = 120;
    private static final int X_PADDING = 20;

    private String alt;
    private String compass;
    private String dist;
    private String fuelPercentStr;
    private double fuelPercent;

    public HudCompassRenderer() {
        super();
    }

    @SubscribeEvent
    public void drawCompassHUD(RenderGameOverlayEvent.Post event) {
        if (event.isCancelable() || MINECRAFT.gameSettings.showDebugInfo || (MINECRAFT.player != null && MINECRAFT.player.onGround)) {
            return;
        }
        if (!isVisible || !MINECRAFT.gameSettings.fullscreen) {
            return;
        }

        if (MINECRAFT.isGameFocused() && event.getType() == RenderGameOverlayEvent.ElementType.ALL) {
            FontRenderer fontRenderer = MINECRAFT.fontRenderer;
            int width = MINECRAFT.getMainWindow().getScaledWidth();

            int rwidth = MINECRAFT.getMainWindow().getWidth();
            int hudX = (rwidth - HUD_WIDTH) - X_PADDING;

            int textX = hudX + (HUD_WIDTH / 2);
            int textY = Y_PADDING + (HUD_HEIGHT / 2);

            Entity chute = null;
            if (MINECRAFT.player.getRidingEntity() instanceof ParachuteEntity) {
                chute = MINECRAFT.player.getRidingEntity();
            } else if (MINECRAFT.player.getRidingEntity() instanceof PoweredParagliderEntity) {
                chute = MINECRAFT.player.getRidingEntity();
            }
            if (chute == null) {
                return;
            }

            BlockPos entityPos = new BlockPos(MINECRAFT.player.getPositionVec().x, MINECRAFT.player.getBoundingBox().minY, MINECRAFT.player.getPositionVec().z);

            altitude = getCurrentAltitude(entityPos);
            double homeDir = getHomeDirection(chute.rotationYaw);
            double distance = getHomeDistance();
            double compassHeading = calcCompassHeading(chute.rotationYaw);
            boolean aadActive = (MINECRAFT.player.getRidingEntity() instanceof PoweredParagliderEntity) ? PoweredParagliderItem.getAADState() : ParachuteItem.getAADState();

            RenderSystem.pushMatrix();

            RenderSystem.enableRescaleNormal();
            RenderSystem.enableBlend();
            RenderSystem.defaultBlendFunc();

            RenderSystem.scaled(0.25, 0.25, 0.25);

            // draw the background
            if (isNightTime()) {
                drawTextureFixed(NIGHT_TEXTURE, hudX);
            }
            drawTextureFixed(BACKGROUND_TEXTURE, hudX);

            // draw the compass ring
            drawTextureWithRotation((float) -compassHeading, COMPASS_TEXTURE, hudX);

            // draw the home direction ring
            drawTextureWithRotation((float) homeDir, HOME_TEXTURE, hudX);

            // draw the "where the hell is the front of the PARACHUTE" color-coded reticle
            // red = not front facing, yellow =  +/-10 degrees, green = +/-2 desgrees
            float playerLook = MathHelper.wrapDegrees(MINECRAFT.player.getRotationYawHead() - chute.rotationYaw);
            if (playerLook <= 2.5 && playerLook >= -2.5) {
                drawTextureFixed(RETICULE_GREEN_TEXTURE, hudX);
            } else if (playerLook <= 15.0 && playerLook >= -15.0) {
                drawTextureFixed(RETICULE_YELLOW_TEXTURE, hudX);
            } else {
                drawTextureFixed(RETICULE_RED_TEXTURE, hudX);
            }

            // damp the update (20 ticks/second modulo 10 is about 1/2 second updates)
            if (count % 10 == 0) {
                alt = formatBold(altitude);
                compass = formatBold(compassHeading);
                dist = formatBold(distance);
            }

            if (chute instanceof PoweredParagliderEntity) {
                fuelPercent = PoweredParagliderItem.getFuelAmountPercent();
                fuelPercentStr = formatBold(fuelPercent);
            }
            count++;

            // scale text up to 50%
            RenderSystem.scaled(2.0, 2.0, 2.0);
            // scale the text coords as well
            textX /= 2;
            textY /= 2;

            int hFont = fontRenderer.FONT_HEIGHT;
            // draw the compass heading text
            drawCenteredString(fontRenderer, compass, textX, textY - (hFont * 2) - 2, COLOR_GREEN);

            // draw the altitude text
            drawCenteredString(fontRenderer, alt, textX, textY - hFont, colorAltitude());

            // draw the distance to the home/spawn point text
            drawCenteredString(fontRenderer, dist, textX, textY + 2, COLOR_GREEN);

            // AAD active indicator
            drawCenteredString(fontRenderer, "§lAUTO", textX, textY + hFont + 4, aadActive ? COLOR_GREEN : COLOR_RED);

            if (chute instanceof PoweredParagliderEntity) {
                // draw the fuel percentage
                drawCenteredString(fontRenderer, fuelPercentStr + "%", textX, textY + (hFont * 2) + 5, fuelPercent > 20.0 ? COLOR_GREEN : COLOR_RED);
            }

            RenderSystem.disableRescaleNormal();
            RenderSystem.disableBlend();

            RenderSystem.popMatrix();
        }
    }


    private boolean isNightTime() {
        long ticks = (MINECRAFT.world != null ? MINECRAFT.world.getDayTime() : 0) % MAX_TICKS;
        return ticks > MOON_RISE && ticks < SUN_RISE;
    }

    // draw a fixed texture
    private void drawTextureFixed(ResourceLocation texture, int screenX) {
        RenderSystem.pushMatrix();

        MINECRAFT.getTextureManager().bindTexture(texture);
        blit(screenX, HudCompassRenderer.Y_PADDING, 0, 0, HudCompassRenderer.HUD_WIDTH, HudCompassRenderer.HUD_HEIGHT);

        RenderSystem.popMatrix();
    }

    // draw a rotating texture
    private void drawTextureWithRotation(float degrees, ResourceLocation texture, int screenX) {
        RenderSystem.pushMatrix();

        float tx = screenX + (HudCompassRenderer.HUD_WIDTH / 2.0f);
        float ty = HudCompassRenderer.Y_PADDING + (HudCompassRenderer.HUD_HEIGHT / 2.0f);
        // translate to center and rotate
        RenderSystem.translated(tx, ty, 0);
        RenderSystem.rotatef(degrees, 0, 0, 1);
        RenderSystem.translated(-tx, -ty, 0);

        MINECRAFT.getTextureManager().bindTexture(texture);
        blit(screenX, HudCompassRenderer.Y_PADDING, 0, 0, HudCompassRenderer.HUD_WIDTH, HudCompassRenderer.HUD_HEIGHT);

        RenderSystem.popMatrix();
    }

    // Minecraft font style codes
    // §k	Obfuscated
    // §l	Bold
    // §m	Strikethrough
    // §n	Underline
    // §o	Italic
    // §r	Reset
    private String formatBold(double d) {
        return String.format("§l%.1f", d);
    }

    private String formatBoldi(int d) {
        return String.format("§l%d", d);
    }

    private double calcCompassHeading(double yaw) {
        return (((yaw + 180.0) % 360) + 360) % 360;
    }

    // difference angle in degrees the chute is facing from the home point.
    // zero degrees means the chute is facing the home point.
    // the home point can be either the world spawn point or a waypoint
    // set by the player in the config.
    private double getHomeDirection(double yaw) {
        BlockPos blockpos = MINECRAFT.world != null ? MINECRAFT.world.getSpawnPoint() : new BlockPos(0.0, 0.0, 0.0);
        Vec3d v = MINECRAFT.player != null ? MINECRAFT.player.getPositionVec() : new Vec3d(0.0, 0.0, 0.0);
        double delta = Math.atan2(blockpos.getZ() - v.z, blockpos.getX() - v.x);
        double relAngle = delta - Math.toRadians(yaw);
        return MathHelper.wrapDegrees(Math.toDegrees(relAngle) - 90.0); // degrees
    }

    // Thanks to Pythagoras we can calculate the distance to home/spawn
    private double getHomeDistance() {
        BlockPos blockpos = MINECRAFT.world != null ? MINECRAFT.world.getSpawnPoint() : new BlockPos(0.0, 0.0, 0.0);
        Vec3d v = MINECRAFT.player != null ? MINECRAFT.player.getPositionVec() : new Vec3d(0.0, 0.0, 0.0);
        double a = Math.pow(blockpos.getZ() - v.z, 2);
        double b = Math.pow(blockpos.getX() - v.x, 2);
        return Math.sqrt(a + b);
    }

    private int colorAltitude() {
        return altitude <= 10.0 ? COLOR_RED : altitude <= 15.0 ? COLOR_YELLOW : COLOR_GREEN;
    }

    // calculate altitude in meters above ground. starting at the entity
    // count down until a non-air block is encountered.
    // only allow altitude calculations in the surface world
    // return a weirdly random number if in nether or end.
    private double getCurrentAltitude(BlockPos entityPos) {
        if (MINECRAFT.world != null && MINECRAFT.world.dimension.isSurfaceWorld()) {
            BlockPos blockPos = new BlockPos(entityPos.getX(), entityPos.getY(), entityPos.getZ());
            while (MINECRAFT.world.isAirBlock(blockPos.down())) {
                blockPos = blockPos.down();
            }
            // calculate the entity's current altitude above the ground
            return (entityPos.getY() - 2.5) - blockPos.getY();
        }
        return 1000.0 * (MINECRAFT.world != null ? MINECRAFT.world.rand.nextGaussian() : 0);
    }

    // toggles HUD visibility using a user defined key, 'H' is default
    public static void toggleHUDVisibility() {
        isVisible = !isVisible;
    }

}
