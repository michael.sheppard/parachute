/*
 * PoweredParagliderRenderer.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.parachute.client.renderer;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import com.parachute.client.model.PoweredParagliderModel;
import com.parachute.common.ParachuteMod;
import com.parachute.common.entity.PoweredParagliderEntity;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.Matrix4f;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;

@OnlyIn(Dist.CLIENT)
public class PoweredParagliderRenderer extends EntityRenderer<PoweredParagliderEntity> {

    protected final PoweredParagliderModel paragliderModel = new PoweredParagliderModel();
    private static final ResourceLocation[] PARAGLIDER_TEXTURES = new ResourceLocation[]{
            new ResourceLocation("textures/block/black_wool.png"),
            new ResourceLocation("textures/block/blue_wool.png"),
            new ResourceLocation("textures/block/brown_wool.png"),
            new ResourceLocation("textures/block/cyan_wool.png"),
            new ResourceLocation("textures/block/gray_wool.png"),
            new ResourceLocation("textures/block/green_wool.png"),
            new ResourceLocation("textures/block/light_blue_wool.png"),
            new ResourceLocation("textures/block/light_gray_wool.png"),
            new ResourceLocation("textures/block/lime_wool.png"),
            new ResourceLocation("textures/block/magenta_wool.png"),
            new ResourceLocation("textures/block/orange_wool.png"),
            new ResourceLocation("textures/block/pink_wool.png"),
            new ResourceLocation("textures/block/purple_wool.png"),
            new ResourceLocation("textures/block/red_wool.png"),
            new ResourceLocation("textures/block/white_wool.png"),
            new ResourceLocation("textures/block/yellow_wool.png"),
            new ResourceLocation(ParachuteMod.MODID + ":textures/block/camo_parachute.png"),
            new ResourceLocation(ParachuteMod.MODID + ":textures/block/rainbow_parachute.png")
    };

    public PoweredParagliderRenderer(EntityRendererManager rm) {
        super(rm);
        shadowSize = 0.0F;
    }

    @Override
    public void render(@Nonnull PoweredParagliderEntity paragliderEntity, float rotationYaw, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer typeBuffer, int packedLight) {
        matrixStack.push();
        matrixStack.translate(0.0D, 0.0D, 0.0D);
        matrixStack.rotate(Vector3f.YN.rotationDegrees(rotationYaw));

        paragliderModel.setRotationAngles(paragliderEntity, partialTicks, 0.0F, -2.5F, 0.0F, 0.0F);
        IVertexBuilder ivertexbuilder = typeBuffer.getBuffer(paragliderModel.getRenderType(getEntityTexture(paragliderEntity)));
        paragliderModel.render(matrixStack, ivertexbuilder, packedLight, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);

        if (paragliderEntity.getControllingPassenger() != null && isThirdPersonView()) {
            PlayerEntity rider = (PlayerEntity) paragliderEntity.getControllingPassenger();
            renderPoweredParagliderCords(matrixStack, typeBuffer, rider);
        }

        matrixStack.pop();

        super.render(paragliderEntity, rotationYaw, partialTicks, matrixStack, typeBuffer, packedLight);
    }

    public void renderPoweredParagliderCords(MatrixStack matrixStack, IRenderTypeBuffer typeBuffer, PlayerEntity rider) {
        final float SCALE = 0.0625f;
        int cordColor = (int) Math.floor((rider.getBrightness() * 0.1) * 256.0);

        // eight section paraglider
        final float[] x = { // left/right
                -50.0f, -3.0f, -50.0f, -3.0f, -47.5f, -3.0f, -47.5f, -3.0f,
                50.0f, 3.0f, 50.0f, 3.0f, 47.5f, 3.0f, 47.5f, 3.0f,
                -32.0f, -3.0f, -32.0f, -3.0f, -16.0f, -3.0f, -16.0f, -3.0f,
                32.0f, 3.0f, 32.0f, 3.0f, 16.0f, 3.0f, 16.0f, 3.0f
        };
        final float[] y = { // up/down
                0.515f, 2.0f, 0.515f, 2.0f, 0.36f, 2.0f, 0.36f, 2.0f,
                0.515f, 2.0f, 0.515f, 2.0f, 0.36f, 2.0f, 0.36f, 2.0f,
                0.1f, 2.0f, 0.1f, 2.0f, 0.018f, 2.0f, 0.018f, 2.0f,
                0.1f, 2.0f, 0.1f, 2.0f, 0.018f, 2.0f, 0.018f, 2.0f
        };
        final float[] z = { // front/back
                -8f, 0f, 8f, 0f, -8f, 0f, 8f, 0f,
                -8f, 0f, 8f, 0f, -8f, 0f, 8f, 0f,
                -8f, 0f, 8f, 0f, -8f, 0f, 8f, 0f,
                -8f, 0f, 8f, 0f, -8f, 0f, 8f, 0f
        };

        matrixStack.push();

        matrixStack.scale(SCALE, -1.0f, SCALE);
        IVertexBuilder iVertexBuilder = typeBuffer.getBuffer(RenderType.LINES);
        Matrix4f matrix4f = matrixStack.getLast().getMatrix();

        for (int k = 0; k < x.length; k++) {
            iVertexBuilder.pos(matrix4f, x[k], y[k], z[k]).color(cordColor, cordColor, cordColor, 255).endVertex();
        }

        matrixStack.pop();
    }

    @Override
    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull PoweredParagliderEntity entity) {
        return PARAGLIDER_TEXTURES[entity.getPoweredParagliderColor().ordinal()];
    }

    private boolean isThirdPersonView() {
        return renderManager.options.thirdPersonView > 0;
    }

}
