/*
 * ParachuteInputEvent.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.client.event;

import com.parachute.common.entity.ParachuteEntity;
import com.parachute.common.entity.PoweredParagliderEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.MovementInput;
import net.minecraftforge.client.event.InputUpdateEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;


@SuppressWarnings("unused")
public class ParachuteInputEvent {

    @SubscribeEvent
    public void inputEvent(InputUpdateEvent event) {
        Entity entity = event.getEntity();
        if (entity instanceof PlayerEntity && entity.isPassenger()) {
            MovementInput input = event.getMovementInput();
            if (entity.getRidingEntity() instanceof ParachuteEntity) {
                ((ParachuteEntity) entity.getRidingEntity()).updateInputs(input.leftKeyDown, input.rightKeyDown, input.forwardKeyDown, input.backKeyDown);
            } else if (entity.getRidingEntity() instanceof PoweredParagliderEntity) {
                ((PoweredParagliderEntity) entity.getRidingEntity()).updateInputs(input.leftKeyDown, input.rightKeyDown, input.forwardKeyDown, input.backKeyDown, input.jump);
            }
        }
    }
}
