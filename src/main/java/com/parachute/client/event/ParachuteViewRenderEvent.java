/*
 * ParachuteViewRenderEvent.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.client.event;

import com.parachute.common.Consts;
import com.parachute.common.entity.ParachuteEntity;
import com.parachute.common.entity.PoweredParagliderEntity;
import com.parachute.common.event.ConfigHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@SuppressWarnings("unused")
public class ParachuteViewRenderEvent {
    private static float rollAngle = 0.0f;
    private static final Minecraft MINECRAFT = Minecraft.getInstance();

    @SubscribeEvent
    public void ParachuteRollEvent(EntityViewRenderEvent.CameraSetup event) {
        if (!ConfigHandler.showRollEffect) {
            return;
        }

        boolean leftTurn = false;
        boolean rightTurn = false;
        double forwardMotion = 0.0;
        boolean thirdPersonView = event.getInfo().isThirdPerson();

        if (MINECRAFT.player != null) {
            if (MINECRAFT.player.getRidingEntity() instanceof ParachuteEntity) {
                leftTurn = ParachuteEntity.leftTurn;
                rightTurn = ParachuteEntity.rightTurn;
                forwardMotion = ParachuteEntity.forwardMotion;
            } else if (MINECRAFT.player.getRidingEntity() instanceof PoweredParagliderEntity) {
                leftTurn = PoweredParagliderEntity.leftTurn;
                rightTurn = PoweredParagliderEntity.rightTurn;
                forwardMotion = PoweredParagliderEntity.forwardMotion;
            }

            if (leftTurn && forwardMotion > Consts.SLIDE_MOMENTUM) {
                if (thirdPersonView) {
                    if (rollAngle < 0) {
                        rollAngle += Consts.ROLL_RATE * Consts.ROLL_RECOVERY_RATE;
                    }
                    rollAngle += Consts.ROLL_RATE;
                } else {
                    if (rollAngle > 0) {
                        rollAngle -= Consts.ROLL_RATE * Consts.ROLL_RECOVERY_RATE;
                    }
                    rollAngle -= Consts.ROLL_RATE;
                }
            }
            if (leftTurn && forwardMotion < Consts.TURN_MOMENTUM) {
                if (rollAngle < 0.0f) {
                    rollAngle += Consts.ROLL_RATE;
                }
                if (rollAngle > 0.0f) {
                    rollAngle -= Consts.ROLL_RATE;
                }
            }
            if (rightTurn && forwardMotion > Consts.SLIDE_MOMENTUM) {
                if (thirdPersonView) {
                    if (rollAngle > 0) {
                        rollAngle -= Consts.ROLL_RATE * Consts.ROLL_RECOVERY_RATE;
                    }
                    rollAngle -= Consts.ROLL_RATE;
                } else {
                    if (rollAngle < 0) {
                        rollAngle += Consts.ROLL_RATE * Consts.ROLL_RECOVERY_RATE;
                    }
                    rollAngle += Consts.ROLL_RATE;
                }
            }
            if (rightTurn && forwardMotion < Consts.TURN_MOMENTUM) {
                if (rollAngle < 0.0f) {
                    rollAngle += Consts.ROLL_RATE;
                }
                if (rollAngle > 0.0f) {
                    rollAngle -= Consts.ROLL_RATE;
                }
            }
            if (!leftTurn && !rightTurn) {
                if (rollAngle < 0.0f) {
                    rollAngle += Consts.ROLL_RATE;
                }
                if (rollAngle > 0.0f) {
                    rollAngle -= Consts.ROLL_RATE;
                }
            }

            rollAngle = MathHelper.clamp(rollAngle, -Consts.MAX_ROLL_ANGLE, Consts.MAX_ROLL_ANGLE);
            event.setRoll(rollAngle);
        }
    }
}
