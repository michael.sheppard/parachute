/*
 * ParachuteModel.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */
package com.parachute.client.model;

import com.google.common.collect.ImmutableList;
import com.parachute.common.entity.ParachuteEntity;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;
import java.util.Arrays;

@OnlyIn(Dist.CLIENT)
public class ParachuteModel extends SegmentedModel<ParachuteEntity> {
    private final ImmutableList<ModelRenderer> parachute_sections;

    public ParachuteModel() {
        ImmutableList.Builder<ModelRenderer> builder = ImmutableList.builder();
        ModelRenderer[] sections = new ModelRenderer[6];
        for (int i = 0; i < sections.length; i++) {
            sections[i] = new ModelRenderer(this, 0, 0).setTextureSize(16, 16);
        }

        int x = 16; // left/right
        int y = 2;  // up/down
        int z = 16; // front/back
        final float d2r = (float) Math.toRadians(1.0);

        sections[0].addBox(-5f, -8.25f, -8f, 4, y, z);
        sections[0].setTextureOffset(6, 0);
        sections[0].setRotationPoint(-36F, 0F, 0F);
        sections[0].rotateAngleZ = 45.0f * d2r;

        sections[1].addBox(-32F, 1.6F, -8F, x, y, z);
        sections[1].rotateAngleZ = 15.0f * d2r;

        sections[2].addBox(-16F, -0.5F, -8F, x, y, z);
        sections[2].rotateAngleZ = 7.5f * d2r;

        sections[3].addBox(0F, -0.5F, -8F, x, y, z);
        sections[3].rotateAngleZ = -7.5f * d2r;

        sections[4].addBox(16F, 1.6F, -8F, x, y, z);
        sections[4].rotateAngleZ = -15.0f * d2r;

        sections[5].addBox(1f, -8.25f, -8f, 4, y, z);
        sections[5].setTextureOffset(6, 0);
        sections[5].setRotationPoint(36F, 0F, 0F);
        sections[5].rotateAngleZ = -45.0f * d2r;

        builder.addAll(Arrays.asList(sections));
        parachute_sections = builder.build();
    }

    @Override
    public void setRotationAngles(@Nonnull ParachuteEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return parachute_sections;
    }
}
