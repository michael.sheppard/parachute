/*
 * ParachuteFlyingSound.java
 *
 *  Copyright (c) 2010 - 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.parachute.client.sound;

import com.parachute.common.entity.ParachuteEntity;
import com.parachute.common.entity.PoweredParagliderEntity;
import net.minecraft.client.audio.TickableSound;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ParachuteFlyingSound extends TickableSound {
    private final ClientPlayerEntity player;
    private int time;

    public ParachuteFlyingSound(ClientPlayerEntity player) {
        super(SoundEvents.ITEM_ELYTRA_FLYING, SoundCategory.PLAYERS);
        this.player = player;
        volume = 0.1f;
        repeat = true;
        repeatDelay = 0;
//        attenuationType = AttenuationType.LINEAR;
    }

    @Override
    public void tick() {
        ++time;
        boolean isRidingParachute = player.getRidingEntity() instanceof ParachuteEntity || player.getRidingEntity() instanceof PoweredParagliderEntity;

        if (player.isAlive() && (time <= 20 || (player.isPassenger() && isRidingParachute))) {
            x = (float)(player.getPositionVec().x - player.prevPosX);
            y = (float)(player.getPositionVec().y - player.prevPosY);
            z = (float)(player.getPositionVec().z - player.prevPosZ);
            float velocity = MathHelper.sqrt(x * x + y * y + z * z);
            if ((double) velocity >= 0.01) {
                volume = MathHelper.clamp(velocity, 0.0f, 1.0f);
            } else {
                volume = 0.0f;
            }
            if (time < 20) {
                volume = 0.0f;
            } else if (time < 40) {
                volume = (float) ((double) volume * ((double) (time - 20) / 20.0));
            }
            if (volume > 0.8f) {
                pitch = 1.0f + (volume - 0.8f);
            } else {
                pitch = 1.0f;
            }
        } else {
            donePlaying = true;
        }
    }
}
